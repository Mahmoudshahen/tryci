var STATUS_MSG = {

    ERROR: {
        LOGIN: {
            WRONG_PASSWORD: "كلمة المرور غير صحيحة",
            WRONG_EMAIL: "البريد الإلكتروني غير موجود",
            USER_UNVERIFIED: "لم يتم تفعيل المستخدم بعد برجاء التحقق من البريد الإلكتروني",
        },
        REGISTER: {
            INVALID_PASSWORD: "لا يجب أن تقل كلمة المرور عن 8 حروف وألا تزيد عن 20 حرف",
            INVALID_MOBILE: "رقم الجوال غير صحيح",
            INVALID_NAME: "أسم المستخدم غير صحيح",
            INVALID_EMAIL: "البريد الإلكتروني غير موجود",
            USED_EMAIL: "البريد الإلكتروني مستخدم بالفعل",
            VERIFY_CODE_EXPIRED: "أنتهت صلاحية كود التفعيل"
            
        },
        LOGOUT: {
            LOGOUT_FAILD: "فشل فى تسجيل الخروج"
        },
        RESET_PASSWORD: {
            CODE_EXPIRED: 'رابط إعاده ضبط كلمة المرور منتهى الصلاحية'
        },
        ADMIN: {
            INVALID_ACCESS: "غير مسموح"
        }
    },
    SUCCESS: {
        LOGOUT: {
            LOGOUT_SUCCESS: "تم تسجيل الخروج بنجاح"
        }
    }
}



module.exports = STATUS_MSG;