/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var QueryExpression = backend.QueryExpression;
var ComparisonOperators = require('beamjs').ComparisonOperators;
var Utils = require('../utils/error_messages.js');

var Promotion = require('../models/promotion.js').promotion;

module.exports.updatepromotion = behaviour({

    name: 'updatepromotion',
    version: '1',
    path: '/updatepromotion',
    method: 'POST',
    type: 'database_with_action',
    parameters: {
        id: {
            key: 'id',
            type: 'body'
        },
        name: {
            key: 'name',
            type: 'body'
        },
        discount: { //  (discount = 10) considered 10%
            key: 'discount',
            type: 'body'
        },
        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        user: {
            key: 'user',
            type: 'middleware'
        }
    },
    returns: {

        updated: {
            key: 'updated',
            type: 'body'
        }
    }
}, function(init) {

    return function() {

        var self = init.apply(this, arguments).self();
        var promotion = null;
        var error = null;
        self.begin('ErrorHandling', function(key, businessController, operation) {
            businessController.modelController.save(function(er) {

                operation.error(function(e) {

                    return error || er || e;
                }).apply();
            });

        });
        if (typeof self.parameters.user.type !== 'number' || self.parameters.user.type !== 0) {

            error = new Error(Utils.ERROR.ADMIN.INVALID_ACCESS);
            error.code = 401;
            return;
        }


        self.begin('Query', function(key, businessController, operation) {

            operation.query([new QueryExpression({
                fieldName: '_id',
                comparisonOperator: ComparisonOperators.EQUAL,
                fieldValue: self.parameters.id
            })]).entity(new Promotion()).callback(function(promotions, e) {

                if (e) error = e;
                promotion = Array.isArray(promotions) && promotions.length === 1 && promotions[0];
                if (promotion) {

                    promotion.name = self.parameters.name;
                    promotion.discount = self.parameters.discount;
                }
            }).apply();

        }).begin('ModelObjectMapping', function(key, businessController, operation) {

            operation.callback(function(response) {

                if (promotion)
                    response.updated = true;

            }).apply();
        });
    };
});