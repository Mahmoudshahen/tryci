/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var Package = require('../models/package.js').package;
var Getcurriculumpromotion = require('./get_curriculum_prmotion.js').getcurriculumpromotion;

module.exports.getcurriculumprice = behaviour({

    name: 'getcurriculumprice',
    version: '1',
    path: '/curruculums/price/in',
    parameters: {

        curriculumid: {
            key: 'curriculumid',
            type: 'body'
        }
    },
    returns: {

        price: {
            key: 'price',
            type: 'body'
        }
    }
}, function(init) {

    return function() {
        var self = init.apply(this, arguments).self();
        var price = {};

        var error = null;
        self.begin('ErrorHandling', function(key, businessController, operation) {

            operation.error(function(e) {

                return error || e;
            }).apply();
        });
        if(typeof self.parameters.curriculumid !== 'number' || self.parameters.curriculumid.length === 0) {
            error = new Error("invalid curriculum id");
            error.code = 401;
            return;
        }

        self.begin('Query', function(key, businessController, operation) {

            operation.query().entity(new Package()).callback(function(packages, e) {

                if (e) error = e;

                if (packages && Array.isArray(packages)) {

                    for (var i = 0; i < packages.length; i++) {

                        for (var j = 0; j < packages[i].curriculums.length; j++) {

                            if (packages[i].curriculums[j] === self.parameters.curriculumid) {

                                price.cost = packages[i].cost;
                                price.currency = packages[i].currency;
                                price.packageid = packages[i]._id;
                                break;
                            }
                        }
                    }
                }



            }).apply();
        }).use(function(key, businessController, next) {

            var getcurriculumpromotion = new Getcurriculumpromotion({
                type: 1,
                priority: 0,
                inputObjects: {
                    curriculumid: self.parameters.curriculumid
                }
            });
            self.mandatoryBehaviour = getcurriculumpromotion;
            businessController.runBehaviour(getcurriculumpromotion, null, function(response, err) {

                if (err) {
                    error = err;
                    return;
                }
                if (response.promotion) {

                    price.promotion = response.promotion;
                    price.costAfterPromotion = price.cost - (price.cost * price.promotion.discount / 100);
                }
                next();
            });

        }).begin(function(key, businessController, operation) {

            operation.callback(function(response) {

                response.price = price;
            }).apply();
        }).when('ModelObjectMapping');
    }
});
