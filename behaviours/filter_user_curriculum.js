/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var Curriculum = require('../models/curriculum.js').curriculum;
var LogicalOperators = require('beamjs').LogicalOperators;
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;

module.exports.filterusercurriculums = behaviour({

    name: 'filterusercurriculums',
    version: '1',
    path: '/filterusercurriculums',
    method: 'POST',
    parameters: {

        curriculumsids: {
            key: 'curriculumsids',
            type: 'body'
        }
    },
    returns: {

        curriculums: {
            key: 'curriculums',
            type: 'body'
        }

    }
}, function (init) {

    return function () {
        var self = init.apply(this, arguments).self();
        var curriculums = [];
        var error = null;
        self.begin('ErrorHandling', function (key, businessController, operation) {

            operation.error(function (e) {

                return error || e;
            }).apply();
        });
        if (!self.parameters.curriculumsids && !Array.isArray(self.parameters.curriculumsids)) {
            error = new Error('invalid curriculums ids');
            error.code = 401;
            return;
        }
        var queryExp = [];
        for (var i = 0; i < self.parameters.curriculumsids.length; i++) {

            queryExp.push(new QueryExpression({
                fieldName: '_id',
                comparisonOperator: ComparisonOperators.EQUAL,
                fieldValue: self.parameters.curriculumsids[i]._id,
                logicalOperator: LogicalOperators.OR,
                contextualLevel: 0
            }));
        }

        self.begin('Query', function (key, businessController, operation) {

            operation.query(queryExp)
                .entity(new Curriculum())
                .callback(function (_curriculums, e) {

                    if (e) error = e;

                    if (Array.isArray(_curriculums)) {
                        for (var i = 0; i < _curriculums.length; i++) {
                            curriculums.push({
                                code: _curriculums[i].code,
                                duration: _curriculums[i].duration,
                                level: _curriculums[i].level,
                                name: _curriculums[i].name,
                                university: _curriculums[i].university,
                                _id: _curriculums[i]._id,
                                media: _curriculums[i].media,
                                features: _curriculums[i].features,
                                instructor: _curriculums[i].instructor,
                                description: _curriculums[i].description,
                                socialmedia: _curriculums[i].social_media,
                                links: _curriculums[i].links,
                                links_v2: _curriculums[i].links_v2,
                                

                            });

                        }
                    }
                }).apply();
        }).begin('ModelObjectMapping', function (key, businessController, operation) {

            operation.callback(function (response) {

                response.curriculums = curriculums;
            }).apply();
        });
    }
});
