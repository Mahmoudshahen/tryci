/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var User = require('../models/user.js').user;
var crypto = require('crypto');

var request_email = require('./email_manager/request_email');

module.exports.resetpassword = behaviour({

    name: 'resetpassword',
    version: '1',
    path: '/resetpassword',
    method: 'POST',
    type: 'database_with_action',
    parameters: {

        email: {
            key: 'email',
            type: 'body'
        },
        host: {
            key: 'hostname',
            type: 'middleware'
        },
        protocol: {
            key: 'protocol',
            type: 'middleware'
        }
    },
    returns: {

        sent: {

            type: 'body'
        }
    }
}, function (init) {

    return function () {

        var self = init.apply(this, arguments).self();
        var user = null;
        var error = null;
        self.begin('ErrorHandling', function (key, businessController, operation) {

            businessController.modelController.save(function (er) {

                operation.error(function (e) {

                    return error || er || e;
                }).apply();
            });

        });

        if (typeof self.parameters.email !== 'string' || self.parameters.email.length === 0) {

            error = new Error('البريد الإلكتروني غير صحيح');
            error.code = 401;
            return;
        }

        self.begin('Query', function (key, businessController, operation) {

            operation.query([new QueryExpression({

                fieldName: 'email',
                comparisonOperator: ComparisonOperators.EQUAL,
                comparisonOperatorOptions: ComparisonOperators.CASEINSENSITIVECOMPARE,
                fieldValue: self.parameters.email
            })]).entity(new User()).callback(function (users, e) {

                if (e) error = e;
                user = Array.isArray(users) && users[0];
                if (user) {
                    user.reset_code = crypto.randomBytes(64).toString('hex');
                    var date = new Date();
                    date.setHours(date.getHours() + 24);
                    user.reset_expiredate = date;
                }else {
                    error = new Error('البريد الإلكتروني غير موجود');
                    error.code = 401;
                    return;
                }
            }).apply();
        }).begin('ModelObjectMapping', function (key, businessController, operation) {

            operation.callback(function (response) {

                if (user) {

                    var email = {
                        to: user.email,
                        subject: "reset password",
                        path: "Reset Password", // path  in folder email template
                        dataObj: {
                            params: "resetcode=" + user.reset_code + "&email=" + user.email,
                            domain: self.parameters.protocol + "://" + self.parameters.host + ":" + request_email.frontend_port
                        }
                    };
                    request_email.send(email, function (res, err) {
                        error = err;
                     });
                    response.sent = true;
                } else {
                    response.sent = false;
                }


            }).apply();
        });
    };
});
