/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var User = require('../models/user.js').user;

module.exports.checkuserhascurriculum = behaviour({
    name: 'checkuserhascurriculum',
    version: '1',
    path: '/checkuserhascurriculum',
    method: 'GET',
    parameters: {

        userid: {
            key: 'userid',
            type: 'body'
        },
        curriculumid: {
            key: 'curriculumid',
            type: 'body'
        }
    },
    returns: {

        has: {
            type: 'body'
        },
        activated: {
            type: 'body'
        }
    }
}, function (init) {

    return function () {
        var self = init.apply(this, arguments).self();
        var has = false;
        var activated = false;
        var error = null;
        self.begin('ErrorHandling', function (key, businessController, operation) {

            operation.error(function (e) {

                return error || e;
            }).apply();
        });

        self.begin('Query', function (key, businessController, operation) {

            operation.query([new QueryExpression({
                fieldName: '_id',
                comparisonOperator: ComparisonOperators.EQUAL,
                fieldValue: self.parameters.userid
            })]).entity(new User()).callback(function (users, e) {

                if (e) { error = e; return; };
                var user = Array.isArray(users) && users.length === 1 && users[0];
                if (user) {
                    for (var i = 0; i < user.curriculums.length; i++) {
                        if (user.curriculums[i]._id === self.parameters.curriculumid) {
                            has = true;
                            if (user.curriculums[i].paid)
                                activated = true;
                            break;
                        }
                    }
                }
            }).apply();
        });

        self.begin('ModelObjectMapping', function (key, businessController, operation) {

            operation.callback(function (response) {

                response.has = has;
                response.activated = activated;
            }).apply();
        });
    }
});
