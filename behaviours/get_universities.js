/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var University = require('../models/university.js').university;
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var LogicalOperators = require('beamjs').LogicalOperators;

module.exports.getuniversities = behaviour({

    name: 'getuniversities',
    version: '1',
    path: '/universities/get',
    method: 'POST',
    parameters: {
        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        user: {
            key: 'user',
            type: 'middleware'
        },
        name: {
            key: 'name',
            type: 'body'
        },
        page: {
            key: 'page',
            type: 'body'
        }
    },
    returns: {

        universities: {
            key: 'universities',
            type: 'body'
        },
        pagecount: {
            type: 'body'
        }
    }
}, function (init) {

    return function () {
        var self = init.apply(this, arguments).self();
        var universities = null;
        var pagecount = 0;
        var error = null;
        self.begin('ErrorHandling', function (key, businessController, operation) {

            operation.error(function (e) {

                return error || e;
            }).apply();
        });
        var queryExp = null;
        if (self.parameters.name) {
            queryExp = [new QueryExpression({
                fieldName: 'name',
                comparisonOperator: ComparisonOperators.EQUAL,
                comparisonOperatorOptions: ComparisonOperators.CASEINSENSITIVECOMPARE,
                fieldValue: new RegExp(self.parameters.name),
                contextualLevel: 0
            }),
            new QueryExpression({
                fieldName: 'email',
                comparisonOperator: ComparisonOperators.EQUAL,
                comparisonOperatorOptions: ComparisonOperators.CASEINSENSITIVECOMPARE,
                fieldValue: new RegExp(self.parameters.name),
                logicalOperator: LogicalOperators.OR,
                contextualLevel: 0
            }),
            new QueryExpression({
                fieldName: 'branch',
                comparisonOperator: ComparisonOperators.EQUAL,
                comparisonOperatorOptions: ComparisonOperators.CASEINSENSITIVECOMPARE,
                fieldValue: new RegExp(self.parameters.name),
                logicalOperator: LogicalOperators.OR,
                contextualLevel: 0
            })];
        }
        var paginate = null;
        if (self.parameters.page && typeof self.parameters.page === 'number') {
            paginate = {
                paginate: true,
                page: self.parameters.page || 1,
                limit: 50,
            };
        }
        self.begin('Query', function (key, businessController, operation) {

            operation.query(queryExp)
                .entity(new University(paginate))
                .callback(function (_universities, e) {

                    if (e) error = e;
                    if (paginate !== null) {
                        pagecount = _universities.pageCount;
                        _universities = _universities.modelObjects;
                    }

                    universities = Array.isArray(_universities) && _universities;
                }).apply();
        });

        self.begin('ModelObjectMapping', function (key, businessController, operation) {

            operation.callback(function (response) {

                if (universities) {
                    response.universities = universities;
                    response.pagecount = pagecount;
                }
            }).apply();
        });
    }
});
