/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var User = require('../models/user.js').user;
var Anonymoususer = require('../models/anonymous_user.js').anonymoususer;
var Getanonymousmessagescategorized = require('./get_anonymous_messages_categorized').getanonymousmessagescategorized;
module.exports.getallusersmessages = behaviour({
  name: 'getallusersmessages',
  version: '1',
  path: '/feedback/getallusersmessages',
  method: 'POST',
  parameters: {

    token: {
      key: 'X-Access-Token',
      type: 'header'
    },
    user: {
      key: 'user',
      type: 'middleware'
    },
    page: {
      key: 'page',
      type: 'body'
    }
  },
  returns: {

    usermessages: {
      type: 'body'
    },
    anonymousmessages: {
      type: 'body'
    },
    pagecount: {
      type: 'body'
    }
  }
}, function (init) {

  return function () {
    var self = init.apply(this, arguments).self();
    var userMessages = [];
    var anonymousMessages = [];
    var pagecount = 0;
    var error = null;
    self.begin('ErrorHandling', function (key, businessController, operation) {

      operation.error(function (e) {

        return error || e;
      }).apply();

    });
    if (typeof self.parameters.user !== 'object' || self.parameters.user.type !== 0) {

      error = new Error('Invalid admin');
      error.code = 401;
      return;
    }
    var paginate = null;
    if (self.parameters.page && typeof self.parameters.page === 'number') {
      paginate = {
        paginate: true,
        page: self.parameters.page || 1,
        limit: 50,
      };
    }

    self.begin('Query', function (key, businessController, operation) {

      operation.query()
        .entity(new User(paginate))
        .callback(function (users, e) {

          if (e) { error = e; return; };
          if (paginate !== null) {
            pagecount = users.pageCount;
            users = users.modelObjects;
          }
          if (users && users.length > 0) {

            for (var j = 0; j < users.length; j++) {

              if (users[j].messages && users[j].messages.length > 0)
                userMessages.push({
                  messages: users[j].messages,
                  userid: users[j]._id,
                  name: users[j].name
                });
            }
          }
          else {
            error = new Error('لا يوجد مستخدم');
            error.code = 401;
          }

        }).apply();
    }).use(function (key, businessController, next) {

      var getanonymousmessagescategorized = new Getanonymousmessagescategorized({
        type: 1,
        priority: 0,
        inputObjects: {
          page: self.parameters.page
        }
      });
      self.mandatoryBehaviour = getanonymousmessagescategorized;
      businessController.runBehaviour(getanonymousmessagescategorized, null, function (response, err) {

        if (err) {
          error = err;
          return;
        }
        if (response.messages) {
          anonymousMessages = response.messages;
          pagecount += response.pagecount;
        }

        next();
      });

    }).begin(function (key, businessController, operation) {

      operation.callback(function (response) {

        response.usermessages = userMessages;
        response.anonymousmessages = anonymousMessages;
        response.pagecount = pagecount;
      }).apply();
    }).when('ModelObjectMapping');
  }
});
