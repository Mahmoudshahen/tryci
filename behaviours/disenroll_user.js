/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var Curriculum = require('../models/curriculum.js').curriculum;

module.exports.disenrolluser = behaviour({

    name: 'disenrolluser',
    version: '1',
    path: '/users/disenrolluser',
    method: 'POST',
    type: 'database_with_action',
    parameters: {

        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        curriculumid: {
            key: 'curriculumid',
            type: 'body'
        },
        user: {
            key: 'user',
            type: 'middleware'
        }
    },
    returns: {

        disenrolled: {

            type: 'body'
        }
    }
}, function (init) {

    return function () {

        var self = init.apply(this, arguments).self();
        var error = null;
        self.begin('ErrorHandling', function (key, businessController, operation) {

            businessController.modelController.save(function (er) {

                operation.error(function (e) {

                    return error || er || e;
                }).apply();
            }, [self.parameters.user]);
        });
        if (typeof self.parameters.curriculumid !== "number" || self.parameters.curriculumid.length === 0) {

            error = new Error('Invalid curriculum Id');
            error.code = 401;
            return;
        }
        self.use(function (key, businessController, next) {

            for (var i = 0; i < self.parameters.user.curriculums.length; i++) {

                if (self.parameters.curriculumid === self.parameters.user.curriculums[i]._id) {
                    self.parameters.user.curriculums.splice(i, 1);
                    break;
                }
            }

            next();
        }).begin(function (key, businessController, operation) {

            operation.callback(function (response) {

                response.disenrolled = self.parameters.user && true;
            }).apply();
        }).when('ModelObjectMapping');
    };
});
