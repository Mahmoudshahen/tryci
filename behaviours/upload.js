var backend = require("beamjs").backend();
var behaviour = backend.behaviour();
var multer = require("multer");
var filesystem = require("fs");
var mkdirp = require('mkdirp');
var pathlib = require('path');

var error = null;

var storage = multer.diskStorage({

  destination: function (req, file, cb) {

    var userdir = "./uploads";
    if (!filesystem.exists(userdir))
      mkdirp(userdir, function (err) {

        if (err) {

          error = new Error("failed to create directory");
          error.code = 300;
          return;
        }
      });
    cb(null, userdir);
  },
  filename: function (req, file, cb) {

    if (!file) {

      error = new Error("error in file");
      error.code = 300;
      return;
    }
    cb(null, file.originalname.toLowerCase());
  }
});

var upload = multer({

  storage: storage
});

module.exports.upload = behaviour({
  name: "upload",
  version: "1",
  path: "/upload",
  method: "POST",
  parameters: {
    token: {
      key: "x-access-token",
      type: "header"
    },
    file: {
      key: "file",
      type: "middleware"
    },
     host: {
      key: 'hostname',
      type: 'middleware'
    },
    protocol: {
      key: 'protocol',
      type: 'middleware'
    }
    // user: {
      // key: "user",
      // type: "middleware"
    // }
  },
  returns: {
    url: {
      type: "body"
    },
    filename: {
      type: "body"
    },
    host: {
      type: "body"

    },
    protocol: {
      type: "body"

    }
  },
  plugin: upload.single("file")
}, function (init) {
  return function () {
    var self = init.apply(this, arguments).self();
    var error = null;
    self.begin("ErrorHandling", function (key, businessController, operation) {
      operation
        .error(function (e) {
          return error || e;
        })
        .apply();
    });
    self.begin("ModelObjectMapping", function (key, businessController, operation) {
      //var extension = self.parameters.file.mimetype.split("/")[1];

      operation
        .callback(function (response) {

          var filepath = typeof self.parameters.file === "object"
            ? self.parameters.file.path.replace("uploads", "")
            : "";
          response.url = filepath !== "" ? filepath.replace("\\", '/') : "";
          response.filename = self.parameters.file.originalname;
          response.host = self.parameters.host;
          response.protocol = self.parameters.protocol;

        })
        .apply();
    });
  };
}
);
