/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var Utils = require('../utils/error_messages.js');
var University = require('../models/university.js').university;

module.exports.adduniversity = behaviour({

    name: 'adduniversity',
    version: '1',
    path: '/adduniversity',
    method: 'POST',
    type: 'database_with_action',
    parameters: {

        name: {
            key: 'name',
            type: 'body'
        },
        mobile: {
            key: 'mobile',
            type: 'body'
        },
        email: {
            key: 'email',
            type: 'body'
        },
        country: {
            key: 'country',
            type: 'body'
        },
        city: {
            key: 'city',
            type: 'body'
        },
        branch: {
            key: 'branch',
            type: 'body'
        },
        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        user: {
            key: 'user',
            type: 'middleware'
        }
    },
    returns: {

        name: {
            key: 'name',
            type: 'body'
        },
        id: {
            key: 'id',
            type: 'body'
        },
        added: {
            key: 'added',
            type: 'body'
        }
    }
}, function(init) {

    return function() {

        var self = init.apply(this, arguments).self();
        var university = null;
        var error = null;
        self.begin('ErrorHandling', function(key, businessController, operation) {
            businessController.modelController.save(function(er) {

                operation.error(function(e) {

                    return error || er || e;
                }).apply();
            });

        });
        if (typeof self.parameters.user.type !== 'number' || self.parameters.user.type !== 0) {

            error = new Error(Utils.ERROR.ADMIN.INVALID_ACCESS);
            error.code = 401;
            return;
        }


        self.begin('Insert', function(key, businessController, operation) {

            var universityObj = {
                name: self.parameters.name,
                mobile: self.parameters.mobile,
                email: self.parameters.email,
                country: self.parameters.country,
                city: self.parameters.city,
                branch: self.parameters.branch
            }

            operation.entity(new University()).objects(universityObj).callback(function(universities, e) {

                if (e) error = e;
            }).apply();

        }).use(function(key, businessController, next) {
            businessController.modelController.save(function(er, res_university) {
                console.log(er);
                if (er) error = er;
                if(res_university)
                    university = res_university[1];
                next();

            });
        }).begin(function(key, businessController, operation) {

            operation.callback(function(response) {

                if (university) {

                    response.name = university.name;
                    response.id = university._id;
                    response.added = true;
                }


            }).apply();
        }).when('ModelObjectMapping');
    };
});
