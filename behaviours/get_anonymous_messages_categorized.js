/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var User = require('../models/user.js').user;
var Anonymoususer = require('../models/anonymous_user.js').anonymoususer;

module.exports.getanonymousmessagescategorized = behaviour({
    name: 'getanonymousmessagescategorized',
    version: '1',
    path: '/feedback/getanonymousmessagescategorized',
    method: 'POST',
    parameters: {

        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        page: {
            key: 'page',
            type: 'body'
        }
    },
    returns: {

        messages: {
            type: 'body'
        },
        pagecount: {
            type: 'body'
        }
    }
}, function (init) {

    return function () {
        var self = init.apply(this, arguments).self();
        var messages = {};
        var messagesArr = [];
        var pagecount = 0.0;
        var error = null;
        self.begin('ErrorHandling', function (key, businessController, operation) {

            operation.error(function (e) {

                return error || e;
            }).apply();

        });

        self.begin('Query', function (key, businessController, operation) {

            operation.query().entity(new Anonymoususer()).callback(function (users, e) {

                if (e) { error = e; return; };

                if (users && users.length > 0 && users[0].messages) {

                    for (var i = 0; i < users[0].messages.length; i++) {

                        if (!messages[users[0].messages[i].sessionid]) {
                            messages[users[0].messages[i].sessionid] = [];

                        }
                        messages[users[0].messages[i].sessionid].push(users[0].messages[i]);

                    }
                    for (var key in messages)
                        messagesArr.push({
                            messages: messages[key],
                            sessionid: key
                        });
                    if (typeof self.parameters.page === 'number' && messagesArr.length > 0) {
                        pagecount = messagesArr.length / 50;
                        messagesArr = messagesArr.splice((self.parameters.page - 1) * 50, 50);
                    }

                }
                else {
                    error = new Error('لا يوجد مستخدم');
                    error.code = 401;
                }

            }).apply();
        });

        self.begin('ModelObjectMapping', function (key, businessController, operation) {

            operation.callback(function (response) {

                response.messages = messagesArr;
                response.pagecount = pagecount;
            }).apply();
        });
    }
});
