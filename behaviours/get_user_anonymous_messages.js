/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var User = require('../models/user.js').user;
var Anonymoususer = require('../models/anonymous_user.js').anonymoususer;

module.exports.getuseranonymousmessages = behaviour({
    name: 'getuseranonymousmessages',
    version: '1',
    path: '/feedback/getuseranonymousmessages',
    method: 'POST',
    parameters: {

        sessionid: {
            key: 'sessionid',
            type: 'body'
        },
        token: {
            key: 'X-Access-Token',
            type: 'header'
        }
    },
    returns: {

        messages: {
            type: 'body'
        }
    }
}, function (init) {

    return function () {
        var self = init.apply(this, arguments).self();
        var messages = [];
        var error = null;
        self.begin('ErrorHandling', function (key, businessController, operation) {

            operation.error(function (e) {

                return error || e;
            }).apply();

        });
        if (typeof self.parameters.sessionid !== 'number' || self.parameters.sessionid.length === 0) {

            error = new Error('invalid session id');
            error.code = 401;
            return;
        }


        self.begin('Query', function (key, businessController, operation) {

            operation.query().entity(new Anonymoususer())
                .callback(function (users, e) {

                    if (e) { error = e; return; };

                    if (users && users.length > 0 && users[0].messages) {

                        for (var i = 0; i < users[0].messages.length; i++) {

                            if (users[0].messages[i].sessionid === self.parameters.sessionid)
                                messages.push(users[0].messages[i]);
                        }


                    }
                    else {
                        error = new Error('لا يوجد مستخدم');
                        error.code = 401;
                    }

                }).apply();
        });

        self.begin('ModelObjectMapping', function (key, businessController, operation) {

            operation.callback(function (response) {

                response.messages = messages;
            }).apply();
        });
    }
});
