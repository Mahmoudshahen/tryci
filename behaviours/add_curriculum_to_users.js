/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var AddCurriculumToUser = require('./add_curriculum_to_user').addcurriculumtouser;
var Utils = require('../utils/error_messages.js');

var addCurriculumToUser = function (self, businessController, users, callback) {

    if (users.length === 0) {
        callback(true, null);
    } else {
        var addcurriculumtouser = new AddCurriculumToUser({

            type: 1,
            priority: 0,
            inputObjects: {
                userid: users[users.length - 1].userid,
                curriculumid: users[users.length - 1].curriculumid,
                devicecompatability: users[users.length - 1].devicecompatability,
                user: self.parameters.user
            }
        });
        self.mandatoryBehaviour = addcurriculumtouser;
        businessController.runBehaviour(addcurriculumtouser, null, function (response, err) {

            if (err) {
                callback(null, err)
            }
            if (response.added) {
                users.pop();
                addCurriculumToUser(self, businessController, users, callback);
            }

        });
    }
}


module.exports.addcurriculumtousers = behaviour({

    name: 'addcurriculumtousers',
    version: '1',
    path: '/users/addcurriculumtousers',
    method: 'POST',
    type: 'database_with_action',
    parameters: {

        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        users: {    //   {userid, curriculumid, devicecompatability}
            key: 'users',
            type: 'body'
        },
        user: {
            key: 'user',
            type: 'middleware'
        }
    },
    returns: {

        added: {

            type: 'body'
        }
    }
}, function (init) {

    return function () {

        var self = init.apply(this, arguments).self();
        var error = null;
        var added = false;
        self.begin('ErrorHandling', function (key, businessController, operation) {

            operation.error(function (e) {

                return error || e;
            }).apply();
        });
        if (typeof self.parameters.user !== 'object' || self.parameters.user.type !== 0) {

            error = new Error(Utils.ERROR.ADMIN.INVALID_ACCESS);
            error.code = 401;
            return;
        }
        if (!self.parameters.users || !Array.isArray(self.parameters.users)) {

            error = new Error('Invalid users array');
            error.code = 401;
            return;
        }
        self.use(function (key, businessController, next) {

            addCurriculumToUser(self, businessController, self.parameters.users, function (response, err) {

                if (err) {
                    error = err;
                    next();
                    return;
                }
                added = true;
                next();
            });


        }).begin(function (key, businessController, operation) {

            operation.callback(function (response) {

                response.added = added;
            }).apply();
        }).when('ModelObjectMapping');
    };
});
