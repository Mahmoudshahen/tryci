/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var User = require('../models/user.js').user;

module.exports.getmostpopularcurriculums = behaviour({

    name: 'getmostpopularcurriculums',
    version: '1',
    path: '/getmostpopularcurriculums',
    method: 'GET',
    type: 'database_with_action',
    parameters: {
        token: {
            key: 'X-Access-Token',
            type: 'header'
        }
    },
    returns: {

        curriculums: {

            type: 'body'
        }
    }
}, function (init) {

    return function () {

        var self = init.apply(this, arguments).self();
        var error = null;
        var mostPopular = [];

        self.begin('ErrorHandling', function (key, businessController, operation) {

            operation.error(function (e) {

                return error || e;
            }).apply();
        });
        self.begin('Query', function (key, businessController, operation) {

            operation.query().entity(new User()).callback(function (users, e) {

                if (e) error = e;
                var curriculumsIdsCount = new Map();

                if (users !== null && users.length > 0) {

                    for (var i = 0; i < users.length; i++) {

                        if (users[i].curriculums) {

                            for (var j = 0; j < users[i].curriculums.length; j++) {

                                if (curriculumsIdsCount.get(users[i].curriculums[j]._id) !== undefined) {

                                    curriculumsIdsCount.set(users[i].curriculums[j]._id, curriculumsIdsCount.get(users[i].curriculums[j]._id) + 1);
                                } else {
                                    curriculumsIdsCount.set(users[i].curriculums[j]._id, 1);
                                }
                            }
                        }

                    }
                    for (var key of curriculumsIdsCount.entries()) {
                        mostPopular.push({_id: key[0], count: key[1]});
                    }
                    mostPopular = mostPopular.sort(function(cur1, cur2){
                        var ret = 0;
                        cur1.value <= cur2.value? ret = 1: ret = -1;
                        return ret;
                    });

                }

            }).apply();
        }).begin('ModelObjectMapping', function (key, businessController, operation) {

            operation.callback(function (response) {

                response.curriculums = mostPopular.splice(0, 5);
            }).apply();
        });
    };
});
