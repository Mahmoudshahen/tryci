/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var Promotion = require('../models/promotion.js').promotion;
var Utils = require('../utils/error_messages.js');

module.exports.addcurriculumtopromotion = behaviour({

    name: 'addcurriculumtopromotion',
    version: '1',
    path: '/promotions/addcurriculum',
    method: 'POST',
    parameters: {

        promotionid: {
            key: 'promotionid',
            type: 'body'
        },
        curriculumid: {
            key: 'curriculumid',
            type: 'body'
        },
        user: {
            key: 'user',
            type: 'middleware'
        },
         token: {
            key: 'X-Access-Token',
            type: 'header'
        },
    },
    returns: {

        added: {
            key: 'added',
            type: 'body'
        }
    }
}, function (init) {

    return function () {

        var self = init.apply(this, arguments).self();
        var promotion = null;
        var error = null;
        self.begin('ErrorHandling', function (key, businessController, operation) {

            businessController.modelController.save(function (er) {

                operation.error(function (e) {

                    return error || er || e;
                }).apply();
            });
        });
        if (typeof self.parameters.user.type !== 'number' || self.parameters.user.type !== 0) {

            error = new Error(Utils.ERROR.ADMIN.INVALID_ACCESS);
            error.code = 401;
            return;
        }
        if (typeof self.parameters.curriculumid !== 'number' || self.parameters.curriculumid.length === 0) {

            error = new Error('Invalid curriculum id');
            error.code = 401;
            return;
        }

        if (typeof self.parameters.promotionid !== 'number' || self.parameters.promotionid.length === 0) {

            error = new Error('Invalid promotion id');
            error.code = 401;
            return;
        }
        self.begin('Query', function (key, businessController, operation) {

            operation.query([new QueryExpression({

                fieldName: '_id',
                comparisonOperator: ComparisonOperators.EQUAL,
                fieldValue: self.parameters.promotionid
            })])
                .entity(new Promotion())
                .callback(function (promotions, e) {

                    if (e) error = e;

                    promotion = Array.isArray(promotions) && promotions.length > 0 && promotions[0];
                    if (promotion) {
                        promotion.curriculums.push({
                            _id: self.parameters.curriculumid
                        });
                    }
                }).apply();
        });

        self.begin('ModelObjectMapping', function (key, businessController, operation) {

            operation.callback(function (response) {

                response.added = true;
            }).apply();
        });
    }
});
