/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var Utils = require('../utils/error_messages.js');
var University = require('../models/university.js').university;

module.exports.adddepartment = behaviour({

    name: 'adddepartment',
    version: '1',
    path: '/adddepartment',
    method: 'POST',
    type: 'database_with_action',
    parameters: {

        name: {
            key: 'name',
            type: 'body'
        },
        collagename: {
            key: 'collagename',
            type: 'body'
        },
        universityid: {
            key: 'universityid',
            type: 'body'
        },
        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        user: {
            key: 'user',
            type: 'middleware'
        }
    },
    returns: {

        name: {
            key: 'name',
            type: 'body'
        },
        id: {
            key: 'id',
            type: 'body'
        },
        added: {
            key: 'added',
            type: 'body'
        }
    }
}, function (init) {

    return function () {

        var self = init.apply(this, arguments).self();
        var university = null;
        var departmentid = null;
        var error = null;
        self.begin('ErrorHandling', function (key, businessController, operation) {

            businessController.modelController.save(function (er) {

                operation.error(function (e) {

                    return error || er || e;
                }).apply();
            });
        });
        if (typeof self.parameters.user.type !== 'number' || self.parameters.user.type !== 0) {

            error = new Error(Utils.ERROR.ADMIN.INVALID_ACCESS);
            error.code = 401;
            return;
        }


        self.begin('Query', function (key, businessController, operation) {


            operation.query([new QueryExpression({
                fieldName: '_id',
                comparisonOperator: ComparisonOperators.EQUAL,
                fieldValue: self.parameters.universityid
            })]).entity(new University()).callback(function (universities, e) {

                if (e) {
                    error = e
                    return;
                };
                university = Array.isArray(universities) && universities.length === 1 && universities[0];

                var date = new Date();

                departmentid = "" + date.getFullYear() + date.getMonth() + date.getDay() + date.getHours() + date.getMinutes() + date.getMilliseconds()

                university.departments.push({

                    _id: parseInt(departmentid),
                    name: self.parameters.name,
                    collage: {
                        name: self.parameters.collagename
                    }
                })


            }).apply();
        }).begin('ModelObjectMapping', function (key, businessController, operation) {

            operation.callback(function (response) {

                if (university) {

                    response.name = self.parameters.name;
                    response.id = departmentid;
                    response.added = true;
                }


            }).apply();
        });
    };
});
