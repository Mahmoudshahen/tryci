/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var Utils = require('../utils/error_messages.js');

var Promotion = require('../models/promotion.js').promotion;

module.exports.addpromotion = behaviour({

    name: 'addpromotion',
    version: '1',
    path: '/addpromotion',
    method: 'POST',
    type: 'database_with_action',
    parameters: {

        name: {
            key: 'name',
            type: 'body'
        },
        discount: { //  (discount = 10) considered 10%
            key: 'discount',
            type: 'body'
        },
        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        user: {
            key: 'user',
            type: 'middleware'
        }
    },
    returns: {

        name: {
            key: 'name',
            type: 'body'
        },
        id: {
            key: 'id',
            type: 'body'
        },
        added: {
            key: 'added',
            type: 'body'
        }
    }
}, function(init) {

    return function() {

        var self = init.apply(this, arguments).self();
        var promotion = null;
        var error = null;
        self.begin('ErrorHandling', function(key, businessController, operation) {
            businessController.modelController.save(function(er) {

                operation.error(function(e) {

                    return error || er || e;
                }).apply();
            });

        });
        if (typeof self.parameters.user.type !== 'number' || self.parameters.user.type !== 0) {

            error = new Error(Utils.ERROR.ADMIN.INVALID_ACCESS);
            error.code = 401;
            return;
        }


        self.begin('Insert', function(key, businessController, operation) {

            var promotionObj = {
                name: self.parameters.name,
                discount: self.parameters.discount
            }

            operation.entity(new Promotion()).objects(promotionObj).callback(function(promotions, e) {

                if (e) error = e;
                promotion = Array.isArray(promotions) && promotions.length === 1 && promotions[0];
            }).apply();

        }).begin('ModelObjectMapping', function(key, businessController, operation) {

            operation.callback(function(response) {

                if (promotion)
                    response.added = true;

            }).apply();
        });
    };
});
