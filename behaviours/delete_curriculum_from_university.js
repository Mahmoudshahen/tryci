/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var University = require('../models/university.js').university;

module.exports.deletecurriculumfromuniversity = behaviour({

    name: 'deletecurriculumfromuniversity',
    version: '1',
    path: "/universities/deletecurriculumfromuniversity/in",
    type: 'database_with_action',
    parameters: {

        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        user: {
            key: 'user',
            type: 'middleware'
        },
        curriculumid: {
            key: 'curriculumid',
            type: 'body'
        },
        universityid: {
            key: 'universityid',
            type: 'body'
        }

    },
    returns: {

        deleted: {

            type: 'body'
        }
    }
}, function (init) {

    return function () {

        var self = init.apply(this, arguments).self();
        var university = null;
        var error = null;

        if (!self.parameters.curriculumid) {
            error = new Error('invalid curriculum id');
            error.code = 300;
            return;
        }
        if (!self.parameters.universityid) {
            error = new Error('invalid university id');
            error.code = 300;
            return;
        }
        self.begin('ErrorHandling', function (key, businessController, operation) {

            businessController.modelController.save(function (er) {

                operation.error(function (e) {

                    return error || er || e;
                }).apply();
            });
        });
        self.begin('Query', function (key, businessController, operation) {

            operation.query([new QueryExpression({

                fieldName: '_id',
                comparisonOperator: ComparisonOperators.EQUAL,
                fieldValue: self.parameters.universityid
            })])
                .entity(new University())
                .callback(function (_universities, e) {

                    if (e) { error = e; return; }
                    university = Array.isArray(_universities) && _universities.length > 0 && _universities[0];
                    if (university && university.departments) {

                        for (var i = 0; i < university.departments.length; i++) {

                            for (var j = 0; j < university.departments[i].curriculums.length; j++) {

                                if (university.departments[i].curriculums[j]._id === self.parameters.curriculumid) {

                                    university.departments[i].curriculums.splice(j, 1);
                                    break;
                                }
                            }
                        }
                    }
                })
                .apply();
        }).begin('ModelObjectMapping', function (key, businessController, operation) {

            operation.callback(function (response) {

                response.deleted = true;
            }).apply();
        });
    };
});
