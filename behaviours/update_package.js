/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var Package = require('../models/package.js').package;
var Utils = require('../utils/error_messages.js');

module.exports.updatepackage = behaviour({

    name: 'updatepackage',
    version: '1',
    path: '/updatepackage',
    method: 'POST',
    type: 'database_with_action',
    parameters: {

        id: {
            key: 'id',
            type: 'body'
        },
        name: {
            key: 'name',
            type: 'body'
        },
        cost: {
            key: 'cost',
            type: 'body'
        },
        currency: {
            key: 'currency',
            type: 'body'
        },
        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        user: {
            key: 'user',
            type: 'middleware'
        }
    },
    returns: {

        updated: {
            key: 'updated',
            type: 'body'
        }
    }
}, function(init) {

    return function() {

        var self = init.apply(this, arguments).self();
        var packageObject = null;
        var error = null;
        self.begin('ErrorHandling', function(key, businessController, operation) {
            businessController.modelController.save(function(er) {

                operation.error(function(e) {

                    return error || er || e;
                }).apply();
            });

        });
        if (typeof self.parameters.user.type !== 'number' || self.parameters.user.type !== 0) {

            error = new Error(Utils.ERROR.ADMIN.INVALID_ACCESS);
            error.code = 401;
            return;
        }


        self.begin('Query', function(key, businessController, operation) {

            operation.query([new QueryExpression({

                fieldName: '_id',
                comparisonOperator: ComparisonOperators.EQUAL,
                fieldValue: self.parameters.id
            })]).entity(new Package()).callback(function(packages, e) {

                if (e) error = e;
                packageObject = Array.isArray(packages) && packages.length === 1 && packages[0];
                if (packageObject) {

                    packageObject.name = self.parameters.name;
                    packageObject.cost = self.parameters.cost;
                    packageObject.currency = self.parameters.currency;
                }
            }).apply();

        }).begin('ModelObjectMapping', function(key, businessController, operation) {

            operation.callback(function(response) {

                if (packageObject) {

                    response.updated = true;
                }


            }).apply();
        });
    };
});