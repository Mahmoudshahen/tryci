/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var User = require('../models/user.js').user;
var Utils = require('../utils/error_messages.js');
var Getserialfromcurriculum = require('./get_serial_from_curriculum').getserialfromcurriculum;
module.exports.activatecurriculum = behaviour({

    name: 'activatecurriculum',
    version: '1',
    path: '/users/activate',
    method: 'POST',
    type: 'database_with_action',
    parameters: {

        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        curriculumid: {
            key: 'curriculumid',
            type: 'body'
        },
        userid: {
            key: 'userid',
            type: 'body'
        },
        serial: {
            key: 'serial',
            type: 'body'
        },
        user: {
            key: 'user',
            type: 'middleware'
        },
        activate: {
            key: 'activate',
            type: 'body'
        }
    },
    returns: {

        activated: {

            type: 'body'
        }
    }
}, function (init) {

    return function () {

        var self = init.apply(this, arguments).self();
        var error = null;
        var activated = false;
        var user = null;
        var userCurriculumIndex = null;
        self.begin('ErrorHandling', function (key, businessController, operation) {

            businessController.modelController.save(function (er) {

                operation.error(function (e) {

                    return error || er || e;
                }).apply();
            }, [user]);
        });
        if (self.parameters.user && self.parameters.user.type !== 0) {

            error = new Error(Utils.ERROR.ADMIN.INVALID_ACCESS);
            error.code = 401;
            return;
        }
        if (typeof self.parameters.curriculumid !== "number" || self.parameters.curriculumid.length === 0) {

            error = new Error('Invalid curriculum Id');
            error.code = 401;
            return;
        }
        if (typeof self.parameters.userid !== "number" || self.parameters.userid.length === 0) {

            error = new Error('Invalid user Id');
            error.code = 401;
            return;
        }
        if (typeof self.parameters.activate !== "number") {

            error = new Error('Invalid activate status');
            error.code = 401;
            return;
        }


        self.begin('Query', function (key, businessController, operation) {

            operation.query([new QueryExpression({

                fieldName: '_id',
                comparisonOperator: ComparisonOperators.EQUAL,
                fieldValue: self.parameters.userid
            })])
                .entity(new User())
                .callback(function (users, e) {

                    if (e) { error = e; return; };
                    if (Array.isArray(users) && users.length === 1) {

                        user = users[0];

                        for (var i = 0; i < user.curriculums.length; i++) {

                            if (user.curriculums[i]._id === self.parameters.curriculumid) {
                                if (self.parameters.activate === 1)
                                    user.curriculums[i].paid = true;
                                else
                                    user.curriculums[i].paid = false;

                                if (self.parameters.serial && self.parameters.serial.length > 0)
                                    user.curriculums[i].serial = self.parameters.serial;
                                activated = user.curriculums[i].paid
                                userCurriculumIndex = i;
                                break;
                            }
                        }
                    }
                })
                .apply();
        }).use(function (key, businessController, next) {

            if (self.parameters.serial || !activated || user.curriculums[userCurriculumIndex].serial) {
                next();
                return false;
            }
            var getserialfromcurriculum = new Getserialfromcurriculum({

                type: 1,
                priority: 0,
                inputObjects: {
                    curriculumid: self.parameters.curriculumid,
                    deviceCompatability: user.curriculums[userCurriculumIndex].deviceCompatability
                }
            });
            self.mandatoryBehaviour = getserialfromcurriculum;
            businessController.runBehaviour(getserialfromcurriculum, null, function (response, err) {

                if (err) {
                    error = err;
                    user.curriculums[userCurriculumIndex].paid = false;
                }
                if (response.serial) {
                    user.curriculums[userCurriculumIndex].serial = response.serial;
                }

                next();
            });

        }).begin(function (key, businessController, operation) {

            operation.callback(function (response) {

                response.activated = activated;
            }).apply();
        }).when('ModelObjectMapping');
    };
});
