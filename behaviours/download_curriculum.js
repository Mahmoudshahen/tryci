/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var zipFolder = require('zip-folder');
var path = require('path');
var fs = require('fs');
var Curriculum = require('../models/curriculum').curriculum;

module.exports.downloadcurriculum = behaviour({

    name: 'downloadcurriculum',
    version: '1',
    path: '/downloadcurriculum',
    method: 'POST',
    parameters: {

        id: {
            key: 'id',
            type: 'body'
        },
        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        user: {
            key: 'user',
            type: 'middleware'
        }
    },
    returns: {

        filepath: {
            key: 'filepath',
            type: 'body'
        }
    }
}, function (init) {

    return function () {
        var self = init.apply(this, arguments).self();
        var curriculum = null;
        var error = null;
        self.begin('ErrorHandling', function (key, businessController, operation) {
            businessController.modelController.save(function (er) {

                operation.error(function (e) {

                    return error || er || e;
                }).apply();
            });
        });
        if (typeof self.parameters.id !== 'number' || self.parameters.id.length === 0) {

            error = new Error("Invalid curriculum id");
            error.code = 401;
            return;
        }

        self.begin('Query', function (key, businessController, operation) {

            operation.query([new QueryExpression({
                fieldName: '_id',
                comparisonOperator: ComparisonOperators.EQUAL,
                fieldValue: self.parameters.id
            })]).entity(new Curriculum({
                include: ['_id', 'name', 'links']
            })).callback(function (curriculums, e) {

                if (e) { error = e; return; }
                curriculum = Array.isArray(curriculums) && curriculums.length > 0 && curriculums[0];


            }).apply();
        }).use(function (key, businessController, next) {
            if (curriculum && curriculum.links && curriculum.links.length > 0) {
                var userdir = "./uploads/" + self.parameters.id;
                if (!fs.existsSync(userdir)) {

                    error = new Error("لا يوجد محتويات");
                    error.code = 401;
                    next();
                    return;
                }
            zipFolder('./uploads/' + self.parameters.id, './uploads/' + self.parameters.id + '/archive.zip', function (err) {
                if (err) {
                    console.log('oh no!', err);
                } else {
                    console.log('EXCELLENT');

                }
                next();
                return;
            });
        } else {
                error = new Error('لا يوجد محتوي');
                error.code = 401;
                next();
                return;
            }
        }).begin(function (key, businessController, operation) {

        operation.callback(function (response) {
            if (curriculum) {
                var filepath = path.resolve('./uploads/' + self.parameters.id + '/archive.zip');
                response.filepath = filepath !== "" ? filepath.replace(/\\/g, '/') : "";
            }
        }).apply();
    }).when('ModelObjectMapping');
    }
});
