/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var Curriculum = require('../models/curriculum.js').curriculum;
var Semaphore = require('await-semaphore').Semaphore;
var semaphore = new Semaphore(1);

module.exports.getserialfromcurriculum = behaviour({

    name: 'getserialfromcurriculum',
    version: '1',
    path: '/getserialfromcurriculum',
    method: 'GET',
    parameters: {

        curriculumid: {
            key: 'curriculumid',
            type: 'body'
        },
        deviceCompatability: {
            key: 'deviceCompatability',
            type: 'body'
        },
        token: {
            key: 'X-Access-Token',
            type: 'header'
        }
    },
    returns: {

        serial: {
            key: 'serial',
            type: 'body'
        }
    }
}, function (init) {

    return function () {
        var self = init.apply(this, arguments).self();
        var serial = null;
        var error = null;
        var curriculums = null;
        self.begin('ErrorHandling', function (key, businessController, operation) {

            businessController.modelController.save(function (er) {

                operation.error(function (e) {

                    return error || er || e;
                }).apply();
            });
        });
        if (typeof self.parameters.curriculumid !== 'number' || self.parameters.curriculumid.length === 0) {
            error = new Error("invalid curriculum id");
            error.code = 401;
            return;
        }
        if (typeof self.parameters.deviceCompatability !== 'string' || self.parameters.deviceCompatability.length === 0) {
            error = new Error("invalid device Compatability");
            error.code = 401;
            return;
        }

        self.begin('Query', function (key, businessController, operation) {

            operation.query([new QueryExpression({
                fieldName: '_id',
                comparisonOperator: ComparisonOperators.EQUAL,
                fieldValue: self.parameters.curriculumid
            })])
                .entity(new Curriculum())
                .callback(function (_curriculums, e) {

                    if (e) error = e;
                    curriculums = _curriculums;
                }).apply();
        }).use(function (key, businessController, next) {

            semaphore.acquire()
                .then(release => {
                    //critical section...
                    if (Array.isArray(curriculums) && curriculums.length === 1) {
                        if (curriculums[0].serials.length > 0 && self.parameters.deviceCompatability === 'win') {
                            serial = curriculums[0].serials.pop();
                        }
                        else if (curriculums[0].serials_mac.length > 0 && self.parameters.deviceCompatability === 'mac') {
                            serial = curriculums[0].serials_mac.pop();
                        }
                        else {
                            error = new Error('لا يوجد سريالات');
                            error.code = 401;
                        }
                    }
                    release();
                    next();
                    return;
                });

        }).begin(function (key, businessController, operation) {

            operation.callback(function (response) {

                response.serial = serial;
            }).apply();
        }).when('ModelObjectMapping');
    }
});
