/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var User = require('../models/user.js').user;
var Utils = require('../utils/error_messages.js');

module.exports.deleteusermessages = behaviour({

    name: 'deleteusermessages',
    version: '1',
    path: '/feedback/deleteusermessages',
    method: 'POST',
    type: 'database_with_action',
    parameters: {

        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        user: {
            key: 'user',
            type: 'middleware'
        },
        userid: {
            key: 'userid',
            type: 'body'
        },

    },
    returns: {

        deleted: {

            type: 'body'
        }
    }
}, function (init) {

    return function () {

        var self = init.apply(this, arguments).self();
        var user = null;
        var error = null;
        self.begin('ErrorHandling', function (key, businessController, operation) {

            businessController.modelController.save(function (er) {

                operation.error(function (e) {

                    return error || er || e;
                }).apply();
            });
        });

        if (typeof self.parameters.userid !== 'number' || self.parameters.userid.length === 0) {

            error = new Error('Invalid user Id');
            error.code = 400;
            return;
        }
        if (typeof self.parameters.user !== 'object' || typeof self.parameters.user._id !== 'number') {

            error = new Error('Invalid user');
            error.code = 401;
            return;
        }
        if (typeof self.parameters.user.type !== 'number' || self.parameters.user.type !== 0) {

            error = new Error(Utils.ERROR.ADMIN.INVALID_ACCESS);
            error.code = 401;
            return;
        }
        self.begin('Query', function (key, businessController, operation) {

            operation.query([new QueryExpression({

                fieldName: '_id',
                comparisonOperator: ComparisonOperators.EQUAL,
                fieldValue: self.parameters.userid
            })])
                .entity(new User())
                .callback(function (users, e) {

                    if (e) error = e;
                    if (Array.isArray(users) && users.length > 0)
                        users[0].messages = undefined;
                })
                .apply();
        }).begin('ModelObjectMapping', function (key, businessController, operation) {

            operation.callback(function (response) {

                response.deleted = true;
            }).apply();
        });
    };
});