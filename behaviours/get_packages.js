/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var Package = require('../models/package.js').package;
var Utils = require('../utils/error_messages.js');
var LogicalOperators = require('beamjs').LogicalOperators;
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;

module.exports.getpackages = behaviour({

    name: 'getpackages',
    version: '1',
    path: '/packages',
    method: 'POST',
    parameters: {

        name: {
            key: 'name',
            type: 'body'
        },
        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        user: {
            key: 'user',
            type: 'middleware'
        },
        page: {
            key: 'page',
            type: 'body'
        }
    },
    returns: {

        packages: {
            key: 'packages',
            type: 'body'
        },
        pagecount: {
            type: 'body'
        }
    }
}, function (init) {

    return function () {
        var self = init.apply(this, arguments).self();
        var packages = [];
        var pagecount = 0;
        var error = null;
        self.begin('ErrorHandling', function (key, businessController, operation) {

            operation.error(function (e) {

                return error || e;
            }).apply();
        });
        if (typeof self.parameters.user.type !== 'number' || self.parameters.user.type !== 0) {

            error = new Error(Utils.ERROR.ADMIN.INVALID_ACCESS);
            error.code = 401;
            return;
        }
        var queryExp = null;
        if (self.parameters.name) {
            queryExp = [new QueryExpression({
                fieldName: 'name',
                comparisonOperator: ComparisonOperators.EQUAL,
                comparisonOperatorOptions: ComparisonOperators.CASEINSENSITIVECOMPARE,
                fieldValue: new RegExp(self.parameters.name),
                contextualLevel: 0
            })];
        }
        var paginate = null;
        if (self.parameters.page && typeof self.parameters.page === 'number') {
            paginate = {
                paginate: true,
                page: self.parameters.page || 1,
                limit: 50,
            };
        }
        self.begin('Query', function (key, businessController, operation) {

            operation.query(queryExp)
                .entity(new Package(paginate))
                .callback(function (res_packages, e) {

                    if (e) error = e;
                    if (paginate !== null) {
                        pagecount = res_packages.pageCount;
                        res_packages = res_packages.modelObjects;
                    }

                    packages = Array.isArray(res_packages) && res_packages;
                }).apply();
        });

        self.begin('ModelObjectMapping', function (key, businessController, operation) {

            operation.callback(function (response) {

                response.packages = packages;
                response.pagecount = pagecount;
            }).apply();
        });
    }
});