/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var User = require('../models/user.js').user;
var Utils = require('../utils/error_messages.js');

module.exports.changepassword = behaviour({

    name: 'changepassword',
    version: '1',
    path: '/user/changepassword',
    method: 'POST',
    parameters: {

        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        user: {
            key: 'user',
            type: 'middleware'
        },
        password: {

            key: 'password',
            type: 'body'
        },
        newpassword: {

            key: 'newpassword',
            type: 'body'
        },
        ip: {

            key: 'ip',
            type: 'middleware'
        }
    },
    returns: {

        email: {

            type: 'body'
        },
        changed: {

            type: 'body'
        }

    }
}, function(init) {

    return function() {

        var self = init.apply(this, arguments).self();
        var user = null;
        var error = null;
        self.begin('ErrorHandling', function(key, businessController, operation) {

            businessController.modelController.save(function(er) {

                operation.error(function(e) {

                    return error || er || e;
                }).apply();
            });
        });
        if (typeof self.parameters.newpassword !== 'string' || self.parameters.newpassword.length === 0) {

            error = new Error(Utils.ERROR.LOGIN.WRONG_PASSWORD);
            error.code = 401;
            return;
        }
        if (typeof self.parameters.password !== 'string' || self.parameters.password.length === 0) {

            error = new Error(Utils.ERROR.LOGIN.WRONG_PASSWORD);
            error.code = 401;
            return;
        }
        self.begin('Query', function(key, businessController, operation) {

            operation.entity(new User()).query([new QueryExpression({

                fieldName: '_id',
                comparisonOperator: ComparisonOperators.EQUAL,
                fieldValue: self.parameters.user._id
            })]).callback(function(users, e) {

                user = Array.isArray(users) && users.length === 1 && users[0];
                if (e) {

                    error = e;
                    return;
                }
                if (user && user.verifyPassword(self.parameters.password)) {
                    user.password = self.parameters.newpassword;
                } else {
                    error = new Error(Utils.ERROR.LOGIN.WRONG_PASSWORD);
                    error.code = 401;
                    return;
                }
            }).apply();
        }).begin('ModelObjectMapping', function(key, businessController, operation) {

            operation.callback(function(response) {

                if (user) {
                    response.email = user.email;
                    response.changed = true;
                }
            }).apply();
        });
    };
});
