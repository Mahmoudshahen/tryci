/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var Curriculum = require('../models/curriculum.js').curriculum;

module.exports.enrolluser = behaviour({

    name: 'enrolluser',
    version: '1',
    path: '/users/enroll',
    method: 'POST',
    type: 'database_with_action',
    parameters: {

        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        curriculumid: {
            key: 'curriculumid',
            type: 'body'
        },
        devicecompatability: {
            key: 'devicecompatability',
            type: 'body'
        },
        user: {
            key: 'user',
            type: 'middleware'
        }
    },
    returns: {

        enrolled: {

            type: 'body'
        }
    }
}, function(init) {

    return function() {

        var self = init.apply(this, arguments).self();
        var error = null;
        self.begin('ErrorHandling', function(key, businessController, operation) {

            businessController.modelController.save(function(er) {

                operation.error(function(e) {

                    return error || er || e;
                }).apply();
            }, [self.parameters.user]);
        });
        if (typeof self.parameters.curriculumid !== "number" || self.parameters.curriculumid.length === 0) {

            error = new Error('Invalid curriculum Id');
            error.code = 401;
            return;
        }
        if (typeof self.parameters.devicecompatability !== "string" || self.parameters.devicecompatability.length === 0) {

            error = new Error('Invalid device compatability');
            error.code = 401;
            return;
        }
        self.begin('Query', function(key, businessController, operation) {

            operation.query([new QueryExpression({

                fieldName: '_id',
                comparisonOperator: ComparisonOperators.EQUAL,
                fieldValue: self.parameters.curriculumid
            })]).entity(new Curriculum()).callback(function(curriculums, e) {

                if (e) error = e;
                if (Array.isArray(curriculums) && curriculums.length === 1) {
                    var curriculum = curriculums[0];
                    for (var i = 0; i < self.parameters.user.curriculums.length; i++) {
                        if (self.parameters.user.curriculums[i]._id === curriculum._id) {
                            error = new Error('مسجل بالفعل');
                            error.code = 405;
                            return;
                        }
                    }
                    self.parameters.user.curriculums.push({
                        _id: curriculum._id,
                        enroll_date: new Date(),
                        paid: false,
                        deviceCompatability: self.parameters.devicecompatability
                        
                    });
                }
            }).apply();
        }).begin('ModelObjectMapping', function(key, businessController, operation) {

            operation.callback(function(response) {

                response.enrolled = self.parameters.user && true;
            }).apply();
        });
    };
});
