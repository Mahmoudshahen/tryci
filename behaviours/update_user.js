/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var User = require('../models/user.js').user;

module.exports.updateuser = behaviour({

    name: 'updateuser',
    version: '1',
    path: '/users/update',
    method: 'POST',
    type: 'database_with_action',
    parameters: {

        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        id: {
            key: 'id',
            type: 'body'
        },
        name: {
            key: 'name',
            type: 'body'
        },
        country: {
            key: 'country',
            type: 'body'
        },
        city: {
            key: 'city',
            type: 'body'
        },
        university: {
            key: 'university',
            type: 'body'
        },
        birthdate: {
            key: 'birthdate',
            type: 'body'
        },
        level: {
            key: 'level',
            type: 'body'
        },
        mobile: {
            key: 'mobile',
            type: 'body'
        },
        user: {
            key: 'user',
            type: 'middleware'
        }
    },
    returns: {

        updated: {

            type: 'body'
        }
    }
}, function (init) {

    return function () {

        var self = init.apply(this, arguments).self();
        var user = null;
        var error = null;

        self.begin('ErrorHandling', function (key, businessController, operation) {

            businessController.modelController.save(function (er) {

                operation.error(function (e) {

                    return error || er || e;
                }).apply();
            }, [self.parameters.user]);
        });

        self.begin('Query', function (key, businessController, operation) {

            operation
                .query([new QueryExpression({

                    fieldName: '_id',
                    comparisonOperator: ComparisonOperators.EQUAL,
                    fieldValue: self.parameters.user._id
                })])
                .entity(new User())
                .callback(function (users, e) {

                    if (e) error = e;

                    user = Array.isArray(users) && users.length === 1 && users[0];

                    self.parameters.user.name = self.parameters.name;
                    self.parameters.user.mobile = self.parameters.mobile;
                    self.parameters.user.country = self.parameters.country;
                    self.parameters.user.city = self.parameters.city;
                    self.parameters.user.university = self.parameters.university;
                    self.parameters.user.birthdate = self.parameters.birthdate;
                    self.parameters.user.level = self.parameters.level;

                })
                .apply();
        })

            .begin('ModelObjectMapping', function (key, businessController, operation) {

                operation.callback(function (response) {

                    response.updated = user && true;
                }).apply();
            });
    };
});
