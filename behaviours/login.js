/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var User = require('../models/user.js').user;
var jwt = require('jsonwebtoken');
var Utils = require('../utils/error_messages.js');

var genToken = function(user, hostname) {

    var expires = expiresIn(365 * 2); // 365 * 2 days
    var token = jwt.sign({

        exp: expires * 24 * 60 * 60,
        jwtid: user._id,
        type: user.type
    }, user.secret, {

        audience: user.email + '/' + hostname
    });

    return token;
};

var expiresIn = function(numDays) {

    var dateObj = new Date();
    return dateObj.setDate(dateObj.getDate() + numDays);
};

module.exports.login = behaviour({

    name: 'login',
    version: '1',
    path: '/login',
    method: 'POST',
    parameters: {


        email: {

            key: 'email',
            type: 'body'
        },
        password: {

            key: 'password',
            type: 'body'
        },
        hostname: {

            key: 'hostname',
            type: 'middleware'
        }
    },
    returns: {

        email: {

            type: 'body'
        },
        name: {

            type: 'body'
        },
        authenticated: {

            type: 'body'
        },
        id: {

            type: 'body'
        },
        type: {

            type: 'body'
        },
        message: {
            type: 'body'
        },
        'X-Access-Token': {

            key: 'token',
            type: 'header',
            purpose: ['constant', {

                as: 'parameter',
                unless: ['login', 'register']
            }]
        }
    }
}, function(init) {

    return function() {

        var self = init.apply(this, arguments).self();
        var user = null;
        var error = null;
        self.begin('ErrorHandling', function(key, businessController, operation) {

            operation.error(function(e) {

                return error || e;
            }).apply();
        });
        if (typeof self.parameters.email !== 'string' || self.parameters.email.length === 0) {

            error = new Error(Utils.ERROR.LOGIN.WRONG_EMAIL);
            error.code = 401;
            return;
        }
        if (typeof self.parameters.password !== 'string' || self.parameters.password.length === 0) {

            error = new Error(Utils.ERROR.LOGIN.WRONG_PASSWORD);
            error.code = 401;
            return;
        }
        self.begin('Query', function(key, businessController, operation) {

            operation.entity(new User()).query([new QueryExpression({

                fieldName: 'email',
                comparisonOperator: ComparisonOperators.EQUAL,
                comparisonOperatorOptions: ComparisonOperators.CASEINSENSITIVECOMPARE,
                fieldValue: self.parameters.email
            })]).callback(function(users, e) {

                user = Array.isArray(users) && users.length === 1 && users[0];
                if (e) {

                    error = e;
                    return;
                }
                if (user && !user.verified) {
                    error = new Error(Utils.ERROR.LOGIN.USER_UNVERIFIED);
                    error.code = 401;
                    return;
                }
            }).apply();
        }).begin('ModelObjectMapping', function(key, businessController, operation) {

            operation.callback(function(response) {

                if (!user) {

                    response.message = Utils.ERROR.LOGIN.WRONG_EMAIL;
                    response.authenticated = false;
                } else if (!user.verifyPassword(self.parameters.password)) {

                    response.message = Utils.ERROR.LOGIN.WRONG_PASSWORD;
                    response.authenticated = false;
                } else {

                    response.authenticated = true;
                    response.email = user.email;
                    response.id = user._id;
                    response.name = user.name;
                    response.type = user.type;
                    response.token = genToken(user, self.parameters.hostname);
                }
            }).apply();
        });
    };
});