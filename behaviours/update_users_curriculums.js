/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var User = require('../models/user.js').user;
var Utils = require('../utils/error_messages.js');
var LogicalOperators = require('beamjs').LogicalOperators;
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;

module.exports.updateuserscurriculums = behaviour({
    name: 'updateuserscurriculums',
    version: '1',
    path: '/updateuserscurriculums',
    method: 'POST',
    parameters: {

        curriculum: {
            key: 'curriculum',
            type: 'body'
        }
    },
    returns: {

        updated: {
            type: 'body'
        }
    }
}, function (init) {

    return function () {
        var self = init.apply(this, arguments).self();
        var res_users = [];
        var error = null;

        if (typeof self.parameters.curriculum !== 'object') {

            error = new Error("invalid curriculum");
            error.code = 401;
            return;
        }
        self.begin('ErrorHandling', function (key, businessController, operation) {

            businessController.modelController.save(function (er) {

                operation.error(function (e) {

                    return error || er || e;
                }).apply();
            });
        });

        self.begin('Query', function (key, businessController, operation) {

            operation.query()
                .entity(new User())
                .callback(function (users, e) {

                    if (e) { error = e; return; }
                    if (Array.isArray(users)) {

                        for (var i = 0; i < users.length; i++) {

                            for (var j = 0; j < users[i].curriculums.length; j++) {
                                if (users[i].curriculums[j]._id === self.parameters.curriculum._id) {
                                    users[i].curriculums[j].name = self.parameters.curriculum.name;
                                    users[i].curriculums[j].level = self.parameters.curriculum.level;
                                    users[i].curriculums[j].code = self.parameters.curriculum.code;
                                    users[i].curriculums[j].links = self.parameters.curriculum.links;
                                    users[i].curriculums[j].duration = self.parameters.curriculum.duration;
                                }
                            }

                        }
                    }
                }).apply();
        });

        self.begin('ModelObjectMapping', function (key, businessController, operation) {

            operation.callback(function (response) {

                response.updated = true;
            }).apply();
        });
    }
});