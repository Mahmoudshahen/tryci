/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var Package = require('../models/package.js').package;
var Utils = require('../utils/error_messages.js');

module.exports.deletecurriculumfrompackage = behaviour({

    name: 'deletecurriculumfrompackage',
    version: '1',
    path: '/packages/deletecurriculum',
    method: 'POST',
    parameters: {

        curriculumid: {
            key: 'curriculumid',
            type: 'body'
        },
        user: {
            key: 'user',
            type: 'middleware'
        },
        token: {
            key: 'X-Access-Token',
            type: 'header'
        }
    },
    returns: {

        deleted: {
            key: 'deleted',
            type: 'body'
        }
    }
}, function (init) {

    return function () {
        var self = init.apply(this, arguments).self();
        var _package = null;
        var error = null;
        self.begin('ErrorHandling', function (key, businessController, operation) {

            businessController.modelController.save(function (er) {

                operation.error(function (e) {

                    return error || er || e;
                }).apply();
            });
        });
        if (typeof self.parameters.user.type !== 'number' || self.parameters.user.type !== 0) {

            error = new Error(Utils.ERROR.ADMIN.INVALID_ACCESS);
            error.code = 401;
            return;
        }
        if (typeof self.parameters.curriculumid !== 'number' || self.parameters.curriculumid.length === 0) {

            error = new Error('Invalid curriculum id');
            error.code = 401;
            return;
        }

        self.begin('Query', function (key, businessController, operation) {

            operation.query().entity(new Package())
                .callback(function (packages, e) {

                    if (e) error = e;

                    _package = Array.isArray(packages) && packages.length > 0 && packages;
                    if (_package) {
                        for (var j = 0; j < _package.length; j++) {

                            for (var i = 0; i < _package[j].curriculums.length; i++) {

                                if (_package[j].curriculums[i] === self.parameters.curriculumid) {
                                    _package[j].curriculums.splice(i, 1);
                                }
                            }
                        }
                    }
                }).apply();
        }).begin('ModelObjectMapping', function (key, businessController, operation) {

            operation.callback(function (response) {

                response.deleted = true;
            }).apply();
        });
    }
});
