/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var Utils = require('../utils/error_messages.js');
var Package = require('../models/package.js').package;

module.exports.addpackage = behaviour({

    name: 'addpackage',
    version: '1',
    path: '/addpackage',
    method: 'POST',
    type: 'database_with_action',
    parameters: {

        name: {
            key: 'name',
            type: 'body'
        },
        cost: {
            key: 'cost',
            type: 'body'
        },
        currency: {
            key: 'currency',
            type: 'body'
        },
        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        user: {
            key: 'user',
            type: 'middleware'
        }
    },
    returns: {

        name: {
            key: 'name',
            type: 'body'
        },
        id: {
            key: 'id',
            type: 'body'
        },
        added: {
            key: 'added',
            type: 'body'
        }
    }
}, function(init) {

    return function() {

        var self = init.apply(this, arguments).self();
        var packageObject = null;
        var error = null;
        self.begin('ErrorHandling', function(key, businessController, operation) {
            businessController.modelController.save(function(er) {

                operation.error(function(e) {

                    return error || er || e;
                }).apply();
            });

        });
        if (typeof self.parameters.user.type !== 'number' || self.parameters.user.type !== 0) {

            error = new Error(Utils.ERROR.ADMIN.INVALID_ACCESS);
            error.code = 401;
            return;
        }


        self.begin('Insert', function(key, businessController, operation) {

            var packageObj = {
                name: self.parameters.name,
                cost: self.parameters.cost,
                type: 'consumable',
                currency: self.parameters.currency
            }

            operation.entity(new Package()).objects(packageObj).callback(function(packages, e) {

                if (e) error = e;
                packageObject = Array.isArray(packages) && packages.length === 1 && packages[0];
            }).apply();

        }).use(function(key, businessController, next) {
            businessController.modelController.save(function(er, res_package) {
                console.log(er);
                if (er) error = er;
                packageObject = res_package[1];
                next();

            });
        }).begin(function(key, businessController, operation) {

            operation.callback(function(response) {

                if (packageObject) {

                    response.name = packageObject.name;
                    response.id = packageObject._id;
                    response.added = true;
                }


            }).apply();
        }).when('ModelObjectMapping');
    };
});
