/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var Curriculum = require('../models/curriculum.js').curriculum;
var Getcurriculumprice = require('./get_curriculum_price.js').getcurriculumprice;
var Checkuserhascurriculum = require('./check_user_has_curriculum.js').checkuserhascurriculum;

module.exports.getcurriculum = behaviour({

    name: 'getcurriculum',
    version: '1',
    path: '/curriculum/get',
    method: 'POST',
    parameters: {

        curriculumid: {
            key: 'curriculumid',
            type: 'body'
        },
        userid: {
            key: 'userid',
            type: 'body'
        },
        token: {
            key: 'X-Access-Token',
            type: 'header'
        }
    },
    returns: {

        curriculum: {
            key: 'curriculum',
            type: 'body'
        },
        registerd: {
            type: 'body'
        },
        activated: {
            type: 'body'
        }
    }
}, function (init) {

    return function () {
        var self = init.apply(this, arguments).self();
        var curriculum = {};
        var price = null;
        var registerd = false;
        var activated = false;
        var error = null;
        self.begin('ErrorHandling', function (key, businessController, operation) {

            operation.error(function (e) {

                return error || e;
            }).apply();
        });
        if (typeof self.parameters.curriculumid !== 'number' || self.parameters.curriculumid.length === 0) {
            error = new Error("invalid curriculum id");
            error.code = 401;
            return;
        }

        self.begin('Query', function (key, businessController, operation) {

            operation.query([new QueryExpression({
                fieldName: '_id',
                comparisonOperator: ComparisonOperators.EQUAL,
                fieldValue: self.parameters.curriculumid
            })])
                .entity(new Curriculum())
                .callback(function (curriculums, e) {

                    if (e) error = e;

                    if (Array.isArray(curriculums) && curriculums.length === 1) {
                        curriculum = {
                            code: curriculums[0].code,
                            duration: curriculums[0].duration,
                            level: curriculums[0].level,
                            name: curriculums[0].name,
                            university: curriculums[0].university,
                            _id: curriculums[0]._id,
                            media: curriculums[0].media,
                            features: curriculums[0].features,
                            instructor: curriculums[0].instructor,
                            description: curriculums[0].description,
                            socialmedia: curriculums[0].social_media
                        };

                    }


                }).apply();
        }).use(function (key, businessController, next) {

            var getcurriculumprice = new Getcurriculumprice({
                type: 1,
                priority: 0,
                inputObjects: {
                    curriculumid: self.parameters.curriculumid
                }
            });
            self.mandatoryBehaviour = getcurriculumprice;
            businessController.runBehaviour(getcurriculumprice, null, function (response, err) {

                if (err) {
                    error = err;
                    return;
                }
                if (response.price) {

                    price = response.price;
                }

                var checkuserhascurriculum = new Checkuserhascurriculum({
                    type: 1,
                    priority: 0,
                    inputObjects: {
                        curriculumid: self.parameters.curriculumid,
                        userid: self.parameters.userid
                    }
                });
                self.mandatoryBehaviour = checkuserhascurriculum;
                businessController.runBehaviour(checkuserhascurriculum, null, function (response, err) {

                    if (err) {
                        error = err;
                    }
                    if (response.has)
                        registerd = true;
                    if (response.activated)
                        activated = true;


                    next();
                });
            });

        }).begin(function (key, businessController, operation) {

            operation.callback(function (response) {

                response.curriculum = {
                    data: curriculum,
                    price: price
                };
                response.registerd = registerd;
                response.activated = activated;
            }).apply();
        }).when('ModelObjectMapping');
    }
});
