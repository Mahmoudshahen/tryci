var backend = require("beamjs").backend();
var behaviour = backend.behaviour();
var multer = require("multer");
var filesystem = require("fs");
var request = require('request');
var csv = require('csvjson');
var Curriculum = require('../models/curriculum').curriculum;
var error = null;
var Utils = require('../utils/error_messages.js');

var storage = multer.diskStorage({

    destination: function (req, file, cb) {

        var userdir = "./uploads";
        if (!filesystem.exists(userdir))
            filesystem.mkdir(userdir, function (err) {

                if (err) {

                    error = new Error("failed to create directory");
                    error.code = 300;
                    return;
                }
            });
        cb(null, userdir);
    },
    filename: function (req, file, cb) {

        if (!file) {

            error = new Error("error in file");
            error.code = 300;
            return;
        }
        cb(null, file.originalname.toLowerCase());
    }
});

var upload = multer({

    storage: storage
});

module.exports.uploadserials = behaviour({

    name: "uploadserials",
    version: "1",
    path: "/uploadserials",
    method: "POST",
    parameters: {

        file: {
            key: "file",
            type: "middleware"
        },
        user: {
            key: "user",
            type: "middleware"
        },
        devicecompatability: {
            key: 'devicecompatability',
            type: 'header'
        },
        host: {
            key: 'hostname',
            type: 'middleware'
        },
        protocol: {
            key: 'protocol',
            type: 'middleware'
        },
        token: {
            key: 'X-Access-Token',
            type: 'header'
        }

    },
    returns: {
        updated: {
            type: "body"
        }
    },
    plugin: upload.single("file")
}, function (init) {
    return function () {
        var self = init.apply(this, arguments).self();
        var error = null;
        var curriculums = [];
        var updated = false;

        self.begin("ErrorHandling", function (key, businessController, operation) {

            businessController.modelController.save(function (er) {

                operation.error(function (e) {

                    return error || er || e;
                }).apply();
            });
        });
        if (!self.parameters.file) {
            error = new Error('لم يتم رفع الملف');
            error.code = 401;
            return;
        }
        if (typeof self.parameters.user !== 'object' || self.parameters.user.type !== 0) {

            error = new Error(Utils.ERROR.ADMIN.INVALID_ACCESS);
            error.code = 401;
            return;
        }
        if (typeof self.parameters.devicecompatability !== 'string' || self.parameters.devicecompatability.length === 0) {

            error = new Error('invalid device compatability');
            error.code = 401;
            return;
        }
        // if(!self.parameters.file.originalname.split('.')[1] === '.csv') {
        //     error = new Error('إمتداد الملف غير صحيح');
        //     error.code = 401;
        //     return;
        // }
        self.begin('Query', function (key, businessController, operation) {

            operation.query().entity(new Curriculum()).callback(function (_curriculums, e) {

                if (e) {
                    error = e;
                    return;
                }
                curriculums = _curriculums;

            }).apply();
        }).use(function (key, businessController, next) {
            var options = {
                delimiter: ',', // optional
                quote: '"' // optional
            };
            var link = self.parameters.protocol + "://" + self.parameters.host + ":8383" + (self.parameters.file.path.replace("uploads", "")).replace(/\\/g, '/');
            request.get(link, function (error, response, body) {

                if (!error && response.statusCode == 200) {

                    csv_data = csv.toObject(body, options);
                    if (csv_data.length > 0 && (!csv_data[0].hasOwnProperty('id') ||
                        !csv_data[0].hasOwnProperty('serial'))) {

                        error = new Error('properties miss match');
                        error.code = 401;
                        next();
                        return;
                    }

                    for (var i = 0; i < csv_data.length; i++) {

                        for (var j = 0; j < curriculums.length; j++) {

                            if (parseInt(csv_data[i].id) === curriculums[j]._id) {
                                if (self.parameters.devicecompatability === 'win') {

                                    var exist = false;
                                    for (var k = 0; k < curriculums[j].serials.length; k++) {

                                        if (csv_data[i].serial === curriculums[j].serials[k]) {
                                            exist = true;
                                            break;
                                        }
                                    }
                                    if (!exist && csv_data[i].serial && csv_data[i].serial.length !== 0)
                                        curriculums[j].serials.push(csv_data[i].serial);
                                }
                                else if (self.parameters.devicecompatability === 'mac') {

                                    var exist = false;
                                    for (var k = 0; k < curriculums[j].serials_mac.length; k++) {

                                        if (csv_data[i].serial === curriculums[j].serials_mac[k]) {
                                            exist = true;
                                            break;
                                        }
                                    }
                                    if (!exist && csv_data[i].serial && csv_data[i].serial.length !== 0)
                                        curriculums[j].serials_mac.push(csv_data[i].serial);
                                }
                                break;
                            }
                        }

                    }
                    updated = true;

                }
                next();

            });
        }).begin(function (key, businessController, operation) {

            operation.callback(function (response) {

                // console.log("ssssssssssss");
                // console.log(updated);
                response.updated = updated;

            }).apply();
        }).when('ModelObjectMapping');
    };
}
);
