/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var Curriculum = require('../models/curriculum.js').curriculum;
var GetUserAdmin = require('./get_user_admin').getuseradmin;
var Utils = require('../utils/error_messages.js');
module.exports.addcurriculumtouser = behaviour({

    name: 'addcurriculumtouser',
    version: '1',
    path: '/users/addcurriculumtouser',
    method: 'POST',
    type: 'database_with_action',
    parameters: {

        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        curriculumid: {
            key: 'curriculumid',
            type: 'body'
        },
        userid: {
            key: 'userid',
            type: 'body'
        },
        devicecompatability: {
            key: 'devicecompatability',
            type: 'body'
        },
        user: {
            key: 'user',
            type: 'middleware'
        }
    },
    returns: {

        added: {

            type: 'body'
        }
    }
}, function (init) {

    return function () {

        var self = init.apply(this, arguments).self();
        var error = null;
        var user = null;
        var curriculum = null;
        var added = false;
        self.begin('ErrorHandling', function (key, businessController, operation) {

            businessController.modelController.save(function (er) {

                operation.error(function (e) {

                    return error || er || e;
                }).apply();
            }, [user]);
        });
        if (typeof self.parameters.user !== 'object' || self.parameters.user.type !== 0) {

            error = new Error(Utils.ERROR.ADMIN.INVALID_ACCESS);
            error.code = 401;
            return;
        }
        if (typeof self.parameters.curriculumid !== "number" || self.parameters.curriculumid.length === 0) {

            error = new Error('Invalid curriculum Id');
            error.code = 401;
            return;
        }
        if (typeof self.parameters.userid !== "number" || self.parameters.userid.length === 0) {

            error = new Error('Invalid user Id');
            error.code = 401;
            return;
        }
        if (typeof self.parameters.devicecompatability !== "string" || self.parameters.devicecompatability.length === 0) {

            error = new Error('Invalid device compatability');
            error.code = 401;
            return;
        }
        self.begin('Query', function (key, businessController, operation) {

            operation.query([new QueryExpression({

                fieldName: '_id',
                comparisonOperator: ComparisonOperators.EQUAL,
                fieldValue: self.parameters.curriculumid
            })]).entity(new Curriculum()).callback(function (curriculums, e) {

                if (e) error = e;
                if (Array.isArray(curriculums) && curriculums.length === 1) {
                    curriculum = curriculums[0];
                }else {
                    error = new Error('المقرر غير موجود');
                    error.code = 401;
                    return;
                }
            }).apply();
        }).use(function(key, businessController, next) {

            var getuseradmin = new GetUserAdmin({

                type: 1,
                priority: 0,
                inputObjects: {
                    userid: self.parameters.userid,
                }
            });
            self.mandatoryBehaviour = getuseradmin;
            businessController.runBehaviour(getuseradmin, null, function (response, err) {

                if (err) {
                    error = err;
                }
                else if (response.user && curriculum) {
                    user = response.user;
                    for (var i = 0; i < user.curriculums.length; i++) {
                        if (user.curriculums[i]._id === curriculum._id) {
                            error = new Error('مسجل بالفعل');
                            error.code = 405;
                            next();
                            return;
                        }
                    }
                    user.curriculums.push({
                        _id: curriculum._id,
                        enroll_date: new Date(),
                        paid: false,
                        deviceCompatability: self.parameters.devicecompatability

                    });
                    added = true;
                }

                next();
            });


        }).begin(function (key, businessController, operation) {

            operation.callback(function (response) {

                response.added = added;
            }).apply();
        }).when('ModelObjectMapping');
    };
});
