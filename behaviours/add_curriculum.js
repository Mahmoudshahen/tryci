/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var Utils = require('../utils/error_messages.js');
var Curriculum = require('../models/curriculum.js').curriculum;
var Getuniversity = require('./get_university.js').getuniversity;
var Addcurriculumtopackage = require('./delete_curriculum_from_package').deletecurriculumfrompackage;


module.exports.addcurriculum = behaviour({

    name: 'addcurriculum',
    version: '1',
    path: '/curriculums/add',
    method: 'POST',
    type: 'database_with_action',
    parameters: {

        level: {
            key: 'level',
            type: 'body'
        },
        code: {
            key: 'code',
            type: 'body'
        },
        name: {
            key: 'name',
            type: 'body'
        },
        subject: {
            key: 'subject',
            type: 'body'
        },
        links: {
            key: 'links',
            type: 'body'
        },
        links_v2: {
            key: 'links_v2',
            type: 'body'
        },
        duration: {
            key: 'duration',
            type: 'body'
        },
        description: {
            key: 'description',
            type: 'body'
        },
        instructor: {
            key: 'instructor',
            type: 'body'
        },
        media: {
            key: 'media',
            type: 'body'
        },
        features: {
            key: 'features',
            type: 'body'
        },
        socialmedia: {
            key: 'socialmedia',
            type: 'body'
        },
        universityid: {

            key: 'universityid',
            type: 'body'
        },
        departmentid: {
            key: 'departmentid',
            type: 'body'
        },
        packageid: {
            key: 'packageid',
            type: 'body'
        },
        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        user: {
            key: 'user',
            type: 'middleware'
        }
    },
    returns: {

        added: {
            key: 'added',
            type: 'body'
        },
        id: {
            key: 'id',
            type: 'body'
        }
    }
}, function (init) {

    return function () {

        var self = init.apply(this, arguments).self();
        var curriculum = null;
        var university = null;
        var error = null;
        if (self.parameters.user.type !== 0) {

            error = new Error(Utils.ERROR.ADMIN.INVALID_ACCESS);
            error.code = 401;
            return;
        }
        if (typeof self.parameters.universityid !== "number" || self.parameters.universityid.length === 0) {

            error = new Error('Invalid university Id');
            error.code = 401;
            return;
        }
        if (typeof self.parameters.departmentid !== "number" || self.parameters.departmentid.length === 0) {

            error = new Error('Invalid department Id');
            error.code = 401;
            return;
        }
        if (typeof self.parameters.packageid !== "number" || self.parameters.packageid.length === 0) {

            error = new Error('Invalid package Id');
            error.code = 401;
            return;
        }

        self.begin('ErrorHandling', function (key, businessController, operation) {

            businessController.modelController.save(function (er) {

                operation.error(function (e) {

                    return error || er || e;
                }).apply();
            }, [university, curriculum]);
        });

        self.begin('Insert', function (key, businessController, operation) {

          
            if (Array.isArray(self.parameters.links_v2)) {
                for (var i = 0; i < self.parameters.links_v2.length; i++) {
                    self.parameters.links_v2[i]._id = 0;
                }
            }
            var curriculumObj = {

                level: self.parameters.level,
                code: self.parameters.code,
                name: self.parameters.name,
                subject: self.parameters.subject,
                links: self.parameters.links,
                links_v2: self.parameters.links_v2 || [],
                duration: self.parameters.duration,
                description: self.parameters.description,
                instructor: self.parameters.instructor,
                media: self.parameters.media,
                features: self.parameters.features,
                social_media: self.parameters.socialmedia,
                university: {

                    id: self.parameters.universityid,
                    name: "",
                    branch: "",
                    department_id: self.parameters.departmentid,
                    department_name: ""
                }

            }


            operation.entity(new Curriculum()).objects(curriculumObj).callback(function (curriculums, e) {

                curriculum = Array.isArray(curriculums) && curriculums.length === 1 && curriculums[0];

                if (e) error = e;
            }).apply();
        }).use(function (key, businessController, next) {

            businessController.modelController.save(function (er, res_curriculum) {

                console.log(er);
                if (er) { error = er; return; }
                curriculum = res_curriculum[1];
                var getuniversity = new Getuniversity({

                    type: 1,
                    priority: 0,
                    inputObjects: {
                        universityid: self.parameters.universityid
                    }
                });
                self.mandatoryBehaviour = getuniversity;
                businessController.runBehaviour(getuniversity, null, function (response, err) {

                    if (err) {
                        error = err;
                        return;
                    }
                    if (response) {
                        university = response.university;
                        if (!university) {
                            error = new Error('Can not find university');
                            error.code = 401;
                            next();
                            return;
                        }
                        for (var i = 0; i < university.departments.length; i++) {

                            if (university.departments[i]._id === self.parameters.departmentid) {

                                university.departments[i].curriculums.push({
                                    _id: curriculum._id,
                                    level: curriculum.level,
                                    code: curriculum.code,
                                    name: curriculum.name
                                });
                                curriculum.university.name = university.name;
                                curriculum.university.branch = university.branch;
                                curriculum.university.department_name = university.departments[i].name
                                break;
                            }
                        }
                        next();

                    }
                    // var addcurriculumtopackage = new Addcurriculumtopackage({

                    //     type: 1,
                    //     priority: 0,
                    //     inputObjects: {
                    //         packageid: self.parameters.packageid,
                    //         curriculumid: curriculum._id,
                    //         user: self.parameters.user
                    //     }
                    // });
                    //                    self.mandatoryBehaviour = addcurriculumtopackage;

                    // businessController.runBehaviour(addcurriculumtopackage, null, function (response, err) {

                    //     if (err) {
                    //         error = err;

                    //     }

                    //     next();
                    // });

                });

            });
        }).begin(function (key, businessController, operation) {

            operation.callback(function (response) {

                if (curriculum) {

                    response.added = true;
                    response.id = curriculum._id;
                }

            }).apply();
        }).when('ModelObjectMapping');
    };
});
