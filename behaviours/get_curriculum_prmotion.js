/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var Promotion = require('../models/promotion.js').promotion;

module.exports.getcurriculumpromotion = behaviour({

    name: 'getcurriculumpromotion',
    version: '1',
    path: '/curruculums/promotion/in',
    parameters: {

        curriculumid: {
            key: 'curriculumid',
            type: 'body'
        }
    },
    returns: {

        promotion: {
            key: 'promotion',
            type: 'body'
        }
    }
}, function(init) {

    return function() {
        var self = init.apply(this, arguments).self();
        var promotion = {};

        var error = null;
        self.begin('ErrorHandling', function(key, businessController, operation) {

            operation.error(function(e) {

                return error || e;
            }).apply();
        });

        self.begin('Query', function(key, businessController, operation) {

            operation.query().entity(new Promotion()).callback(function(promotions, e) {

                if (e) error = e;

                if (promotions && Array.isArray(promotions)) {

                    for (var i = 0; i < promotions.length; i++) {

                        for (var j = 0; j < promotions[i].curriculums.length; j++) {

                            if (promotions[i].curriculums[j] === self.parameters.curriculumid) {

                                promotion.discount = promotions[i].discount;
                                promotion.name = promotions[i].name;
                                promotion.id = promotions[i]._id;
                                break;
                            }
                        }
                    }
                }



            }).apply();
        });

        self.begin('ModelObjectMapping', function(key, businessController, operation) {

            operation.callback(function(response) {

                response.promotion = promotion;
            }).apply();
        });
    }
});
