/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var University = require('../models/university.js').university;
var Deleteuniversityincurriculum = require('./delete_university_in_curriculum').deleteuniversityincurriculum;
var deleteDepartment = function (self, businessController, curriculums, callback) {

    if (curriculums.length === 0) {
        callback();
    } else {
        var deleteuniversityincurriculum = new Deleteuniversityincurriculum({
            type: 1,
            priority: 0,
            inputObjects: {
                curriculumid: curriculums[0]._id
            }
        });
        self.mandatoryBehaviour = deleteuniversityincurriculum;
        businessController.runBehaviour(deleteuniversityincurriculum, null, function (response, err) {

            if (err) {
                error = err;
                return;
            }
            curriculums.shift();
            deleteDepartment(self, businessController, curriculums, callback);
        });
    }
}
module.exports.deletefromuniversity = behaviour({

    name: 'deletefromuniversity',
    version: '1',
    path: "/universities/delete/in",
    type: 'database_with_action',
    parameters: {

        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        user: {
            key: 'user',
            type: 'middleware'
        },
        university: {
            key: 'university',
            type: 'body'
        },

    },
    returns: {

        deleted: {

            type: 'body'
        }
    }
}, function (init) {

    return function () {

        var self = init.apply(this, arguments).self();
        var university = self.parameters.university;
        var error = null;

        if (!self.parameters.university) {
            error = new Error('invalid university');
            return;
        }
        self.begin('ErrorHandling', function (key, businessController, operation) {

            businessController.modelController.save(function (er) {

                operation.error(function (e) {

                    return error || er || e;
                }).apply();
            });
        });
        self.use(function (key, businessController, next) {

            var curriculumsShouldDelete = [];
            if (university) {
                for (var i = 0; i < university.departments.length; i++) {

                    curriculumsShouldDelete = curriculumsShouldDelete.concat(university.departments[i].curriculums);
                }
                deleteDepartment(self, businessController, curriculumsShouldDelete, function () {

                    next();

                });
            }



        }).begin(function (key, businessController, operation) {

            operation.query([new QueryExpression({

                fieldName: '_id',
                comparisonOperator: ComparisonOperators.EQUAL,
                fieldValue: self.parameters.university._id
            })])
                .entity(new University())
                .callback(function (_universities, e) {

                    if (e) { error = e; return; }
                })
                .apply();
        }).when('Delete').
            begin('ModelObjectMapping', function (key, businessController, operation) {

                operation.callback(function (response) {

                    response.deleted = true;
                }).apply();
            });
    };
});
