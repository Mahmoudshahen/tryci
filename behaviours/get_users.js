/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var User = require('../models/user.js').user;
var Utils = require('../utils/error_messages.js');
var LogicalOperators = require('beamjs').LogicalOperators;
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var Filterusercurriculums = require('./filter_user_curriculum').filterusercurriculums;

var filterUserCurriculums = function (self, businessController, users, callback, responseUsers) {

    if (users.length === 0) {
        callback(responseUsers);
    } else {
        var filterusercurriculums = new Filterusercurriculums({
            type: 1,
            priority: 0,
            inputObjects: {
                curriculumsids: users[0].curriculums
            }
        });
        self.mandatoryBehaviour = filterusercurriculums;
        businessController.runBehaviour(filterusercurriculums, null, function (response, err) {

            if (err) {
                error = err;
                return;
            }
            responseUsers.push(users[0]);
            if (response.curriculums) {

                for (var i = 0; i < response.curriculums.length; i++) {

                    for (var j = 0; j < responseUsers[responseUsers.length - 1].curriculums.length; j++) {

                        if (response.curriculums[i]._id === responseUsers[responseUsers.length - 1].curriculums[j]._id) {
                            var paid = responseUsers[responseUsers.length - 1].curriculums[j].paid;
                            var enroll_date = responseUsers[responseUsers.length - 1].curriculums[j].enroll_date;
                            var serial = responseUsers[responseUsers.length - 1].curriculums[j].serial;
                            responseUsers[responseUsers.length - 1].curriculums[j] = {};
                            responseUsers[responseUsers.length - 1].curriculums[j].paid = paid;
                            responseUsers[responseUsers.length - 1].curriculums[j].enroll_date = enroll_date;
                            responseUsers[responseUsers.length - 1].curriculums[j].serial = serial;
                            responseUsers[responseUsers.length - 1].curriculums[j]._id = response.curriculums[i]._id;

                            responseUsers[responseUsers.length - 1].curriculums[j].code = response.curriculums[i].code;
                            responseUsers[responseUsers.length - 1].curriculums[j].duration = response.curriculums[i].duration;
                            responseUsers[responseUsers.length - 1].curriculums[j].level = response.curriculums[i].level;
                            responseUsers[responseUsers.length - 1].curriculums[j].name = response.curriculums[i].name;
                            responseUsers[responseUsers.length - 1].curriculums[j].media = response.curriculums[i].media;
                            responseUsers[responseUsers.length - 1].curriculums[j].instructor = response.curriculums[i].instructor;
                            break;
                        }

                    }
                }

            }
            users.shift();
            filterUserCurriculums(self, businessController, users, callback, responseUsers);
        });
    }
}

module.exports.getusers = behaviour({
    name: 'getusers',
    version: '1',
    path: '/admin/users',
    method: 'POST',
    parameters: {

        name: {
            key: 'name',
            type: 'body'
        },
        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        user: {
            key: 'user',
            type: 'middleware'
        },
        verified: {
            key: 'verified',
            type: 'body'
        },
        activated: {
            key: 'activated',
            type: 'body'
        },
        curriculumid: {
            key: 'curriculumid',
            type: 'body'
        },
        page: {
            key: 'page',
            type: 'body'
        }
    },
    returns: {

        users: {
            type: 'body'
        },
        pagecount: {
            type: 'body'
        }
    }
}, function (init) {

    return function () {
        var self = init.apply(this, arguments).self();
        var res_users = [];
        var pagecount = 0;
        var error = null;
        self.begin('ErrorHandling', function (key, businessController, operation) {

            operation.error(function (e) {

                return error || e;
            }).apply();
        });
        if (typeof self.parameters.user !== 'object' || self.parameters.user.type !== 0) {

            error = new Error(Utils.ERROR.ADMIN.INVALID_ACCESS);
            error.code = 401;
            return;
        }

        var queryExp = [];
        var contextualLev = 0;
        if(self.parameters.verified || self.parameters.curriculumid) {
            contextualLev = 1;
        }
        if (self.parameters.name) {
            queryExp = [new QueryExpression({
                fieldName: 'name',
                comparisonOperator: ComparisonOperators.EQUAL,
                comparisonOperatorOptions: ComparisonOperators.CASEINSENSITIVECOMPARE,
                fieldValue: new RegExp(self.parameters.name),
                logicalOperator: LogicalOperators.OR,
                contextualLevel: contextualLev
            }),
            new QueryExpression({
                fieldName: 'email',
                comparisonOperator: ComparisonOperators.EQUAL,
                comparisonOperatorOptions: ComparisonOperators.CASEINSENSITIVECOMPARE,
                fieldValue: new RegExp(self.parameters.name),
                logicalOperator: LogicalOperators.OR,
                contextualLevel: contextualLev
            }),
            new QueryExpression({
                fieldName: 'mobile',
                comparisonOperator: ComparisonOperators.CONTAINS,
                comparisonOperatorOptions: ComparisonOperators.CASEINSENSITIVECOMPARE,
                fieldValue: new RegExp(self.parameters.name),
                logicalOperator: LogicalOperators.OR,
                contextualLevel: contextualLev
            })];
        }
        var paginate = null;
        if (self.parameters.page && typeof self.parameters.page === 'number') {
            paginate = {
                paginate: true,
                page: self.parameters.page || 1,
                limit: 50,
            };
        }
        if (self.parameters.verified !== undefined) {
            var verify = self.parameters.verified === true || self.parameters.verified === 1 ? true : false;

            queryExp.push(new QueryExpression({
                fieldName: 'verified',
                comparisonOperator: ComparisonOperators.EQUAL,
                fieldValue: verify,
                logicalOperator: LogicalOperators.AND,
                contextualLevel: 0
            }));
        }
        if (self.parameters.curriculumid !== undefined) {
            
            queryExp.push(new QueryExpression({
                fieldName: 'curriculums',
                comparisonOperator: ComparisonOperators.ANY,
                fieldValue: { '_id': self.parameters.curriculumid },
                logicalOperator: LogicalOperators.AND,
                contextualLevel: 0
            }))
        }

        self.begin('Query', function (key, businessController, operation) {

            operation.query(queryExp)
                .entity(new User(paginate))
                .callback(function (users, e) {

                    if (e) { error = e; return; }

                    if (paginate !== null) {
                        pagecount = users.pageCount;
                        users = users.modelObjects;
                    }

                    if (Array.isArray(users)) {

                        for (var i = 0; i < users.length; i++) {

                            res_users.push({

                                _id: users[i]._id,
                                email: users[i].email,
                                name: users[i].name,
                                mobile: users[i].mobile,
                                type: users[i].type,
                                country: users[i].country,
                                city: users[i].city,
                                image_uri: users[i].image_uri,
                                university: users[i].university,
                                birthdate: users[i].birthdate,
                                level: users[i].level,
                                curriculums: users[i].curriculums,
                                verified: users[i].verified
                            });
                        }
                    }
                }).apply();
        }).use(function (key, businessController, next) {

            if (res_users && Array.isArray(res_users) && res_users.length > 0) {

                filterUserCurriculums(self, businessController, res_users, function (users) {

                    if (self.parameters.activated !== undefined) {
                        var activate = self.parameters.activated === true || self.parameters.activated === 1 ? true : false;

                        res_users = [];
                        for (var i = 0; i < users.length; i++) {

                            var activatedUser = false;
                            for (var j = 0; j < users[i].curriculums.length; j++) {

                                if (users[i].curriculums[j].paid) {

                                    activatedUser = true;
                                    break;
                                }
                            }
                            if (activate && activatedUser)
                                res_users.push(users[i]);

                            else if (!activate && !activatedUser)
                                res_users.push(users[i]);

                        }
                    } else {
                        res_users = users;
                    }

                    next();
                }, []);
            } else {
                next();
            }

        }).begin(function (key, businessController, operation) {

            operation.callback(function (response) {

                response.users = res_users;
                response.pagecount = pagecount;
            }).apply();
        }).when('ModelObjectMapping');
    }
});