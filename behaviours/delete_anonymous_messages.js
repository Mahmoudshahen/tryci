/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var AnonymousUser = require('../models/anonymous_user').anonymoususer;
var Utils = require('../utils/error_messages.js');

module.exports.deleteuseranonymousmessages = behaviour({

    name: 'deleteuseranonymousmessages',
    version: '1',
    path: '/feedback/deleteuseranonymousmessages',
    method: 'POST',
    type: 'database_with_action',
    parameters: {

        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        user: {
            key: 'user',
            type: 'middleware'
        },
        sessionid: {
            key: 'sessionid',
            type: 'body'
        },

    },
    returns: {

        deleted: {

            type: 'body'
        }
    }
}, function (init) {

    return function () {

        var self = init.apply(this, arguments).self();
        var user = null;
        var error = null;
        self.begin('ErrorHandling', function (key, businessController, operation) {

            businessController.modelController.save(function (er) {

                operation.error(function (e) {

                    return error || er || e;
                }).apply();
            });
        });

        if (typeof self.parameters.sessionid !== 'number' || self.parameters.sessionid.length === 0) {

            error = new Error('Invalid session Id');
            error.code = 400;
            return;
        }
        if (typeof self.parameters.user !== 'object' || typeof self.parameters.user._id !== 'number') {

            error = new Error('Invalid user');
            error.code = 401;
            return;
        }
        if (typeof self.parameters.user.type !== 'number' || self.parameters.user.type !== 0) {

            error = new Error(Utils.ERROR.ADMIN.INVALID_ACCESS);
            error.code = 401;
            return;
        }

        self.begin('Query', function (key, businessController, operation) {

            operation.query()
                .entity(new AnonymousUser())
                .callback(function (users, e) {

                    if (e) error = e;
                    if (Array.isArray(users) && users.length > 0) {
                         
                        for (var i = 0; i < users[0].messages.length; i++) {

                            if(users[0].messages[i].sessionid === self.parameters.sessionid) {
                                users[0].messages.splice(i--, 1);
                                
                            }

                        }
                    }

                })
                .apply();
        }).begin('ModelObjectMapping', function (key, businessController, operation) {

            operation.callback(function (response) {

                response.deleted = true;
            }).apply();
        });
    };
});