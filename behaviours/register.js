/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var validator = require('email-validator');
var LogicalOperators = require('beamjs').LogicalOperators;
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var User = require('../models/user.js').user;
var Utils = require('../utils/error_messages.js');
var crypto = require('crypto');

var request_email = require('./email_manager/request_email');

module.exports.register = behaviour({

    name: 'register',
    version: '1',
    path: '/register',
    method: 'POST',
    type: 'database_with_action',
    parameters: {

        name: {
            key: 'name',
            type: 'body'
        },
        mobile: {
            key: 'mobile',
            type: 'body'
        },
        email: {
            key: 'email',
            type: 'body'
        },
        password: {
            key: 'password',
            type: 'body'
        },
        host: {
            key: 'hostname',
            type: 'middleware'
        },
        protocol: {
            key: 'protocol',
            type: 'middleware'
        }
    },
    returns: {

        email: {

            type: 'body'
        },
        name: {

            type: 'body'
        },
        registered: {

            type: 'body'
        }
    }
}, function(init) {

    return function() {

        var self = init.apply(this, arguments).self();
        var user = null;
        var error = null;
        self.begin('ErrorHandling', function(key, businessController, operation) {

            businessController.modelController.save(function(er) {

                operation.error(function(e) {

                    return error || er || e;
                }).apply();
            });
        });

        if (typeof self.parameters.email !== 'string' || self.parameters.email.length === 0) {

            error = new Error(Utils.ERROR.REGISTER.INVALID_EMAIL);
            error.code = 401;
            return;
        }
        if (typeof self.parameters.name !== 'string' || self.parameters.name.length === 0) {

            error = new Error(Utils.ERROR.REGISTER.INVALID_NAME);
            error.code = 401;
            return;
        }
        if (typeof self.parameters.mobile !== 'string' || self.parameters.mobile.length === 0) {

            error = new Error(Utils.ERROR.REGISTER.INVALID_MOBILE);
            error.code = 401;
            return;
        }

        if (typeof self.parameters.password !== 'string' || self.parameters.password.length < 8 || self.parameters.password.length > 20) {

            error = new Error(Utils.ERROR.REGISTER.INVALID_PASSWORD);
            error.code = 401;
            return;
        }
        self.begin('Query', function(key, businessController, operation) {

                operation
                    .query([new QueryExpression({

                        fieldName: 'email',
                        comparisonOperator: ComparisonOperators.EQUAL,
                        comparisonOperatorOptions: ComparisonOperators.CASEINSENSITIVECOMPARE,
                        fieldValue: self.parameters.email
                    })])
                    .entity(new User())
                    .callback(function(users, e) {

                        if (e) error = e;
                        user = Array.isArray(users) && users[0];
                    })
                    .apply();
            })
            .if(function() {

                if (user) {

                    error = new Error(Utils.ERROR.REGISTER.USED_EMAIL);
                    error.code = 401;
                }
                return !user && !error && true;
            }).begin('Insert', function(key, businessController, operation) {

                var userObj = {
                    name: self.parameters.name,
                    mobile: self.parameters.mobile,
                    email: self.parameters.email,
                    password: self.parameters.password,
                    type: 1,
                    verify_code: crypto.randomBytes(64).toString('hex'),
                    verified: false
                }


                operation.entity(new User()).objects(userObj).callback(function(users, e) {

                    user = Array.isArray(users) && users.length === 1 && users[0];

                    if (e) error = e;
                }).apply();
            }).begin('ModelObjectMapping', function(key, businessController, operation) {

                operation.callback(function(response) {

                    if (user) {
                        response.email = user.email;
                        response.registered = user && true;
                        response.name = user.name;

                        var email = {
                            to: user.email,
                            subject: "Verify Email",
                            path: "VerifyEmail", // path  in foldr email template
                            dataObj: {
                                params: "verify_code=" + user.verify_code + "&email=" + user.email,
                                domain: self.parameters.protocol + "://" + self.parameters.host + ":" + request_email.frontend_port
                            }
                        };
                        request_email.send(email, function(res, err) {
                            error = err;
                        });
                    }


                }).apply();
            });
    };
});