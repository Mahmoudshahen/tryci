/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var University = require('../models/university.js').university;
var Utils = require('../utils/error_messages.js');

module.exports.updateuniversity = behaviour({

    name: 'updateuniversity',
    version: '1',
    path: '/updateuniversity',
    method: 'POST',
    type: 'database_with_action',
    parameters: {

        name: {
            key: 'name',
            type: 'body'
        },
        mobile: {
            key: 'mobile',
            type: 'body'
        },
        email: {
            key: 'email',
            type: 'body'
        },
        country: {
            key: 'country',
            type: 'body'
        },
        city: {
            key: 'city',
            type: 'body'
        },
        branch: {
            key: 'branch',
            type: 'body'
        },
        id: {
            key: 'id',
            type: 'body'
        },
        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        user: {
            key: 'user',
            type: 'middleware'
        }
    },
    returns: {

        name: {
            key: 'name',
            type: 'body'
        },
        id: {
            key: 'id',
            type: 'body'
        },
        updated: {
            key: 'updated',
            type: 'body'
        }
    }
}, function(init) {

    return function() {

        var self = init.apply(this, arguments).self();
        var university = null;
        var error = null;
        self.begin('ErrorHandling', function(key, businessController, operation) {
            businessController.modelController.save(function(er) {

                operation.error(function(e) {

                    return error || er || e;
                }).apply();
            });

        });
        if (typeof self.parameters.user.type !== 'number' || self.parameters.user.type !== 0) {

            error = new Error(Utils.ERROR.ADMIN.INVALID_ACCESS);
            error.code = 401;
            return;
        }

        self.begin('Query', function(key, businessController, operation) {

            operation.query([new QueryExpression({
                fieldName: '_id',
                comparisonOperator: ComparisonOperators.EQUAL,
                fieldValue: self.parameters.id
            })]).entity(new University()).callback(function(universities, e) {

                if (e) error = e;
                university = Array.isArray(universities) && universities.length === 1 && universities[0];
                if (university) {
                    if (self.parameters.name)
                        university.name = self.parameters.name;
                    if (self.parameters.mobile)
                        university.mobile = self.parameters.mobile;
                    if (self.parameters.email)
                        university.email = self.parameters.email;
                    if (self.parameters.country)
                        university.country = self.parameters.country;
                    if (self.parameters.city)
                        university.city = self.parameters.city;
                    if (self.parameters.branch)
                        university.branch = self.parameters.branch;
                }

            }).apply();

        }).begin('ModelObjectMapping', function(key, businessController, operation) {

            operation.callback(function(response) {

                if (university) {

                    response.name = university.name;
                    response.id = university._id;
                    response.updated = true;
                }


            }).apply();
        });
    };
});