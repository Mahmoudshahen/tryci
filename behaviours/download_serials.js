/*jslint node: true*/
'use strict';
var fs = require('fs');
var path = require('path');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
var backend = require('beamjs').backend();
var nodeJsZip = require("nodeJs-zip");
var behaviour = backend.behaviour();
var Utils = require('../utils/error_messages.js');

var Curriculum = require('../models/curriculum').curriculum;

module.exports.downloadserials = behaviour({

    name: 'downloadserials',
    version: '1',
    path: '/downloadserials',
    method: 'POST',
    parameters: {

        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        user: {
            key: 'user',
            type: 'middleware'
        },
        host: {
            key: 'hostname',
            type: 'middleware'
        },
        protocol: {
            key: 'protocol',
            type: 'middleware'
        }
    },
    returns: {

        filepath: {
            key: 'filepath',
            type: 'body'
        },
        host: {
            type: "body"

        },
        protocol: {
            type: "body"

        }
    }
}, function (init) {

    return function () {
        var self = init.apply(this, arguments).self();
        var res_user = null;
        var error = null;
        self.begin('ErrorHandling', function (key, businessController, operation) {
            businessController.modelController.save(function (er) {

                operation.error(function (e) {

                    return error || er || e;
                }).apply();
            });
        });
        if (typeof self.parameters.user.type !== 'number' || self.parameters.user.type.length === 0) {

            error = new Error(Utils.ERROR.ADMIN.INVALID_ACCESS);
            error.code = 401;
            return;
        }

        self.begin('Query', function (key, businessController, operation) {

            operation.query().entity(new Curriculum({
                include: ['_id', 'university.name', 'university.department_name', 'university.branch', 'name', 'serials', 'serials_mac']
            })).callback(function (curriculums, e) {

                if (e) { error = e; return; }
                var exportedCurriculumsWin = [];
                var exportedCurriculumsMac = [];
                for (var i = 0; i < curriculums.length; i++) {

                    if (curriculums[i].serials && curriculums[i].serials.length > 0) {
                        for (var j = 0; j < curriculums[i].serials.length; j++) {
                            exportedCurriculumsWin.push({
                                id: curriculums[i]._id,
                                serial: curriculums[i].serials[j] || "",
                                university: curriculums[i].university.name || "",
                                branch: curriculums[i].university.branch,
                                department: curriculums[i].university.department_name || "",
                                name: curriculums[i].name
                            });
                        }
                    } else {
                        exportedCurriculumsWin.push({
                            id: curriculums[i]._id,
                            serial: "",
                            university: curriculums[i].university.name || "",
                            branch: curriculums[i].university.branch,
                            department: curriculums[i].university.department_name || "",
                            name: curriculums[i].name
                        });
                    }

                    if (curriculums[i].serials_mac && curriculums[i].serials_mac.length > 0) {
                        for (var j = 0; j < curriculums[i].serials_mac.length; j++) {
                            exportedCurriculumsMac.push({
                                id: curriculums[i]._id,
                                serial: curriculums[i].serials_mac[j] || "",
                                university: curriculums[i].university.name || "",
                                branch: curriculums[i].university.branch,
                                department: curriculums[i].university.department_name || "",
                                name: curriculums[i].name
                            });
                        }
                    } else {
                        exportedCurriculumsMac.push({
                            id: curriculums[i]._id,
                            serial: "",
                            university: curriculums[i].university.name || "",
                            branch: curriculums[i].university.branch,
                            department: curriculums[i].university.department_name || "",
                            name: curriculums[i].name
                        });
                    }
                }
                var userdir = "./uploads";
                if (!fs.existsSync(userdir)) {
                    fs.mkdir(userdir, function (err) {

                        if (err) {

                            error = new Error("failed to create directory");
                            error.code = 300;
                            return;
                        }

                    });
                }
                const csvWriterWin = createCsvWriter({
                    path: './uploads/win_serials.csv',
                    header: [
                        { id: 'id', title: 'id' },
                        { id: 'serial', title: 'serial' },
                        { id: 'university', title: 'university' },
                        { id: 'branch', title: 'branch' },
                        { id: 'department_name', title: 'department_name' },
                        { id: 'name', title: 'name' }
                    ]
                });
                const csvWriterMac = createCsvWriter({
                    path: './uploads/mac_serials.csv',
                    header: [
                        { id: 'id', title: 'id' },
                        { id: 'serial', title: 'serial' },
                        { id: 'university', title: 'university' },
                        { id: 'branch', title: 'branch' },
                        { id: 'department_name', title: 'department_name' },
                        { id: 'name', title: 'name' }
                    ]
                });

                csvWriterWin.writeRecords(exportedCurriculumsWin)       // returns a promise
                    .then(() => {
                        csvWriterMac.writeRecords(exportedCurriculumsMac)       // returns a promise
                        .then(() => {
                            var macFile = path.join(__dirname+"/../uploads", "/mac_serials.csv");
                            var winFfile = path.join(__dirname+"/../uploads", "/win_serials.csv");
                            nodeJsZip.zip([winFfile, macFile], {
                                dir: __dirname+"/../uploads",
                                name: "courses"
                            });
                        });
                    });
               


            }).apply();
        });

        self.begin('ModelObjectMapping', function (key, businessController, operation) {

            operation.callback(function (response) {

                response.filepath = self.parameters.protocol + '://' + self.parameters.host + ":8383" + '/courses.zip';
            }).apply();
        });
    }
});
