/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var User = require('../models/user.js').user;
var Filterusercurriculums = require('./filter_user_curriculum').filterusercurriculums;

module.exports.getusercurriculums = behaviour({

    name: 'getusercurriculums',
    version: '1',
    path: '/user/curriculums',
    method: 'GET',
    parameters: {
        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        user: {
            key: 'user',
            type: 'middleware'
        }
    },
    returns: {

        curriculums: {
            type: 'body'
        }
    }
}, function (init) {

    return function () {
        var self = init.apply(this, arguments).self();
        var curriculums = [];
        var userCurriculums = [];
        var error = null;
        self.begin('ErrorHandling', function (key, businessController, operation) {

            operation.error(function (e) {

                return error || e;
            }).apply();
        });

        self.begin('Query', function (key, businessController, operation) {

            operation.query([new QueryExpression({
                fieldName: '_id',
                comparisonOperator: ComparisonOperators.EQUAL,
                fieldValue: self.parameters.user._id
            })]).entity(new User()).callback(function (users, e) {

                if (e) error = e;

                userCurriculums = Array.isArray(users) && users.length === 1 && users[0].curriculums;


            }).apply();
        }).use(function (key, businessController, next) {

            if (userCurriculums && Array.isArray(userCurriculums) && userCurriculums.length > 0) {

                var filterusercurriculums = new Filterusercurriculums({
                    type: 1,
                    priority: 0,
                    inputObjects: {
                        curriculumsids: userCurriculums
                    }
                });
                self.mandatoryBehaviour = filterusercurriculums;
                businessController.runBehaviour(filterusercurriculums, null, function (response, err) {

                    if (err) {
                        error = err;
                    }
                    if (response.curriculums) {
                        for (var i = 0; i < userCurriculums.length; i++) {

                            for (var j = 0; j < response.curriculums.length; j++) {

                                if (userCurriculums[i]._id === response.curriculums[j]._id) {

                                    curriculums.push({

                                        code: response.curriculums[j].code,
                                        duration: response.curriculums[j].duration,
                                        level: response.curriculums[j].level,
                                        name: response.curriculums[j].name,
                                        university: response.curriculums[j].university,
                                        socialmedia: response.curriculums[j].socialmedia,
                                        enroll_date: userCurriculums[i].enroll_date,
                                        paid: userCurriculums[i].paid,
                                        _id: userCurriculums[i]._id,
                                        links: response.curriculums[j].links,
                                        links_v2: response.curriculums[j].links_v2,

                                    });
                                    if (!curriculums[i].paid) {
                                        // curriculums[i].links = [];
                                        // curriculums[i].links_v2 = [];
                                        curriculums[i].serial = "";
                                    } else {

                                        curriculums[i].serial = userCurriculums[i].serial;
                                    }
                                    break;
                                }
                            }

                        }
                    }


                    next();
                });

            } else {
                next();
            }
        }).begin(function (key, businessController, operation) {

            operation.callback(function (response) {

                response.curriculums = curriculums;
            }).apply();
        }).when('ModelObjectMapping');
    }
});
