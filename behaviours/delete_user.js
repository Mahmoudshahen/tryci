/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var User = require('../models/user.js').user;
var Utils = require('../utils/error_messages.js');

module.exports.deleteuser = behaviour({

    name: 'deleteuser',
    version: '1',
    path: '/users/delete',
    method: 'POST',
    type: 'database_with_action',
    parameters: {

        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        user: {
            key: 'user',
            type: 'middleware'
        },
        id: {
            key: 'id',
            type: 'body'
        },

    },
    returns: {

        deleted: {

            type: 'body'
        }
    }
}, function (init) {

    return function () {

        var self = init.apply(this, arguments).self();
        var user = null;
        var error = null;
        self.begin('ErrorHandling', function (key, businessController, operation) {

            businessController.modelController.save(function (er) {

                operation.error(function (e) {

                    return error || er || e;
                }).apply();
            });
        });

        if (typeof self.parameters.id !== 'number' || self.parameters.id === 0) {

            error = new Error('Invalid Id');
            error.code = 400;
            return;
        }
        if (typeof self.parameters.user !== 'object' || typeof self.parameters.user._id !== 'number') {

            error = new Error('Invalid user');
            error.code = 401;
            return;
        }
        if (typeof self.parameters.user.type !== 'number' || self.parameters.user.type !== 0) {

            error = new Error(Utils.ERROR.ADMIN.INVALID_ACCESS);
            error.code = 401;
            return;
        }
        if (self.parameters.user._id === self.parameters.id) {

            error = new Error('لا يمكن مسح المشرف');
            error.code = 401;
            return;
        }
        self.begin('Delete', function (key, businessController, operation) {

            operation.query([new QueryExpression({

                fieldName: '_id',
                comparisonOperator: ComparisonOperators.EQUAL,
                fieldValue: self.parameters.id
            })])
                .entity(new User())
                .callback(function (users, e) {

                    if (e) error = e;
                })
                .apply();
        }).begin('ModelObjectMapping', function (key, businessController, operation) {

            operation.callback(function (response) {

                response.deleted = true;
            }).apply();
        });
    };
});