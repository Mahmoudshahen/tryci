require('./validate.js');

require('./login.js');
require('./logout.js');
require('./register.js');
require('./reply_message');
require('./reset_password');
require('./activate_curriculum.js');

require('./add_curriculum_to_package');
require('./add_curriculum_to_promotion');
require('./add_curriculum_to_users');
require('./add_curriculum.js');
require('./add_department.js');
require('./add_departments.js');

require('./add_package.js');
require('./add_preference.js');
require('./add_promotion.js');
require('./add_university.js');
require('./add_walkthrough_step');
require('./change_password.js');
require('./check_walkthrough_step');
require('./complete_register.js');
require('./complete_reset_password');
require('./delete_anonymous_messages');
require('./delete_curriculum_from_package');
require('./delete_curriculum.js');
require('./delete_department.js');
require('./delete_package.js');
require('./delete_promotion.js');
require('./delete_university.js');
require('./delete_user_messages.js');
require('./delete_user.js');
require('./disenroll_user');
require('./download_curriculum');
require('./download_serials');

require('./enroll_user.js');
require('./get_all_users_messages');

require('./get_curriculum_admin');
require('./get_curriculum.js');
require('./get_curriculums.js');
require('./get_home_content.js');
require('./get_level_categorized.js');
require('./get_packages.js');
require('./get_preference.js');
require('./get_promotions.js');
require('./get_universities');
require('./get_university.js');
require('./get_unregistered_users');
require('./get_user_anonymous_messages');

require('./get_user_curriculums');
require('./get_user_messages_admin');
require('./get_user_messages');

require('./get_user.js');
require('./get_users.js');
require('./download_serials.js');
require('./search_curriculums.js');

require('./send_message_anonymous');

require('./send_message.js');
require('./update_curriculum.js');
require('./update_package.js');
require('./update_preference.js');
require('./update_promotion');
require('./update_university.js');
require('./update_user.js');

require('./upload_serials_csv');
require('./upload');
require('./verify_user');