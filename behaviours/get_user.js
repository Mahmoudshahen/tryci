/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var User = require('../models/user.js').user;

module.exports.getuser = behaviour({
  name: 'getuser',
  version: '1',
  path: '/users/userprofile',
  method: 'GET',
  parameters: {
    token: {
      key: 'X-Access-Token',
      type: 'header'
    },
    user: {
      key: 'user',
      type: 'middleware'
    }
  },
  returns: {

    user: {
      type: 'body'
    }
  }
}, function (init) {

  return function () {
    var self = init.apply(this, arguments).self();
    var res_user = null;
    var error = null;
    self.begin('ErrorHandling', function (key, businessController, operation) {

      operation.error(function (e) {

        return error || e;
      }).apply();
    });

    self.begin('Query', function (key, businessController, operation) {

      operation.query([new QueryExpression({
        fieldName: '_id',
        comparisonOperator: ComparisonOperators.EQUAL,
        fieldValue: self.parameters.user._id
      })])
        .entity(new User())
        .callback(function (users, e) {

          if (e) { error = e; return; };
          if (Array.isArray(users) && users.length > 0) {

            res_user = {
              _id: users[0]._id,
              email: users[0].email,
              name: users[0].name,
              mobile: users[0].mobile,
              type: users[0].type,
              country: users[0].country,
              city: users[0].city,
              image_uri: users[0].image_uri,
              university: users[0].university,
              birthdate: users[0].birthdate,
              level: users[0].level
            }
          }
        }).apply();
    });

    self.begin('ModelObjectMapping', function (key, businessController, operation) {

      operation.callback(function (response) {

        response.user = res_user;
      }).apply();
    });
  }
});
