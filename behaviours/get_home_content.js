/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var Curriculum = require('../models/curriculum.js').curriculum;
var GetMostPopularCurriculums = require('./get_most_popular_curriculums').getmostpopularcurriculums;
var FilterUserCurriculums = require('./filter_user_curriculum').filterusercurriculums;
module.exports.gethomecontent = behaviour({

    name: 'gethomecontent',
    version: '1',
    path: '/home',
    method: 'GET',
    type: 'database_with_action',
    parameters: {
        token: {
            key: 'X-Access-Token',
            type: 'header'
        }
    },
    returns: {

        content: {

            type: 'body'
        }
    }
}, function (init) {

    return function () {

        var self = init.apply(this, arguments).self();
        var error = null;
        var content = null;

        self.begin('ErrorHandling', function (key, businessController, operation) {

            operation.error(function (e) {

                return error || e;
            }).apply();
        });
        self.begin('Query', function (key, businessController, operation) {

            operation.query().entity(new Curriculum()).callback(function (Curriculums, e) {

                if (e) error = e;
                var curriculumsNames = [];
                var universities = [];
                var levels = [];
                if (Curriculums !== null && Curriculums.length > 0) {

                    var curriculumsNames_map = new Map();
                    var universities_map = new Map();
                    var levels_map = new Map();

                    for (var i = 0; i < Curriculums.length; i++) {

                        curriculumsNames_map.set(Curriculums[i].name, {

                            name: Curriculums[i].name
                        });
                        universities_map.set(Curriculums[i].university.name + Curriculums[i].university.branch, {

                            name: Curriculums[i].university.name,
                            branch: Curriculums[i].university.branch,
                            id: Curriculums[i].university.id
                        });
                        levels_map.set(Curriculums[i].level, {
                            level: Curriculums[i].level
                        });
                    }

                    for (var key of curriculumsNames_map.entries()) {
                        curriculumsNames.push(key[1]);
                    }
                    for (var key of universities_map.entries()) {
                        universities.push(key[1]);
                    }
                    for (var key of levels_map.entries()) {
                        levels.push(key[1]);
                    }

                }
                content = {

                    curriculumsNames: curriculumsNames,
                    universities: universities,
                    levels: levels
                }
            }).apply();
        }).use(function (key, businessController, next) {

            var getmostpopularcurriculums = new GetMostPopularCurriculums({
                type: 1,
                priority: 0,
                inputObjects: {

                }
            });
            self.mandatoryBehaviour = getmostpopularcurriculums;
            businessController.runBehaviour(getmostpopularcurriculums, null, function (response, err) {

                if (err) {
                    error = err;
                    next();
                    return;
                }
                if (response.curriculums) {
                
                var filterusercurriculums = new FilterUserCurriculums({
                    type: 1,
                    priority: 0,
                    inputObjects: {
                        curriculumsids: response.curriculums
                    }
                });
                self.mandatoryBehaviour = filterusercurriculums;
                businessController.runBehaviour(filterusercurriculums, null, function (response, err) {
    
                    if (err) {
                        error = err;
                        next();
                        return;
                    }
                    if (response.curriculums) {
                        content['mostpopular'] = response.curriculums.map(cur => cur.name);
                    }
    
                    next();
                });
            }else {
                next();
            }
    
            });

        }).begin(function (key, businessController, operation) {

            operation.callback(function (response) {

                response.content = content;
            }).apply();
        }).when('ModelObjectMapping');
    };
});
