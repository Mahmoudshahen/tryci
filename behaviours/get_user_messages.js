/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var User = require('../models/user.js').user;
var Anonymoususer = require('../models/anonymous_user.js').anonymoususer;

module.exports.getusermessages = behaviour({
  name: 'getusermessages',
  version: '1',
  path: '/feedback/getusermessages',
  method: 'POST',
  parameters: {

    token: {
      key: 'X-Access-Token',
      type: 'header'
    },
    user: {
      key: 'user',
      type: 'middleware'
    }
  },
  returns: {

    messages: {
      type: 'body'
    }
  }
}, function (init) {

  return function () {
    var self = init.apply(this, arguments).self();
    var messages = [];
    var error = null;
    self.begin('ErrorHandling', function (key, businessController, operation) {

      operation.error(function (e) {

        return error || e;
      }).apply();

    });
    if (typeof self.parameters.user !== 'object') {
      error = new Error('invalid user');
      error.code = 300;
      return;
    }


    self.begin('Query', function (key, businessController, operation) {

      operation.query([new QueryExpression({
        fieldName: '_id',
        comparisonOperator: ComparisonOperators.EQUAL,
        fieldValue: self.parameters.user._id
      })])
        .entity(new User())
        .callback(function (users, e) {

          if (e) { error = e; return; };

          if (users && users.length > 0) {
        
              messages = users[0].messages;
          }
          else {
            error = new Error('لا يوجد مستخدم');
            error.code = 404;
            return;
          }

        }).apply();
    });

    self.begin('ModelObjectMapping', function (key, businessController, operation) {

      operation.callback(function (response) {

        response.messages = messages;
      }).apply();
    });
  }
});
