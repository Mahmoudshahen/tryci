/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var University = require('../models/university.js').university;
var Deletedepartmentincurriculum = require('./delete_department_in_curriculum').deletedepartmentincurriculum;
var Utils = require('../utils/error_messages.js');

var deleteDepartment = function (self, businessController, curriculums, callback) {

    if (curriculums.length === 0) {
        callback();
    } else {
        var deletedepartmentincurriculum = new Deletedepartmentincurriculum({
            type: 1,
            priority: 0,
            inputObjects: {
                curriculumid: curriculums[0]._id
            }
        });
        self.mandatoryBehaviour = deletedepartmentincurriculum;
        businessController.runBehaviour(deletedepartmentincurriculum, null, function (response, err) {

            if (err) {
                error = err;
                return;
            }
            curriculums.shift();
            deleteDepartment(self, businessController,curriculums, callback);
        });
    }
}
module.exports.deletedepartment = behaviour({

    name: 'deletedepartment',
    version: '1',
    path: '/university/department/delete',
    method: 'POST',
    type: 'database_with_action',
    parameters: {

        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        user: {
            key: 'user',
            type: 'middleware'
        },
        departmentid: {
            key: 'departmentid',
            type: 'body'
        },
        universityid: {
            key: 'universityid',
            type: 'body'
        }

    },
    returns: {

        deleted: {
            key: 'deleted',
            type: 'body'
        }
    }
}, function (init) {

    return function () {

        var self = init.apply(this, arguments).self();
        var university = null;
        var error = null;
        self.begin('ErrorHandling', function (key, businessController, operation) {

            businessController.modelController.save(function (er) {

                operation.error(function (e) {

                    return error || er || e;
                }).apply();
            }, [university]);
        });

        if (typeof self.parameters.universityid !== 'number' || self.parameters.universityid.length === 0) {

            error = new Error('Invalid university Id');
            error.code = 400;
            return;
        }
        if (typeof self.parameters.departmentid !== 'number' || self.parameters.departmentid.length === 0) {

            error = new Error('Invalid department Id');
            error.code = 400;
            return;
        }
        if (typeof self.parameters.user.type !== 'number' || self.parameters.user.type !== 0) {

            error = new Error(Utils.ERROR.ADMIN.INVALID_ACCESS);
            error.code = 401;
            return;
        }
        self.begin('Query', function (key, businessController, operation) {

            operation.query([new QueryExpression({

                fieldName: '_id',
                comparisonOperator: ComparisonOperators.EQUAL,
                fieldValue: self.parameters.universityid
            })])
                .entity(new University())
                .callback(function (_universities, e) {

                    if (e) {
                        error = e;
                        return;
                    }
                    if (Array.isArray(_universities) && _universities.length > 0)
                        university = _universities[0];
                    else {
                        error = new Error('not exist');
                        return;
                    }

                })
                .apply();
        }).use(function (key, businessController, next) {
            var departmentIndex = null;
            if (university) {

                for (var i = 0; i < university.departments.length; i++) {

                    if (university.departments[i]._id === self.parameters.departmentid) {
                        departmentIndex = i;
                        deleteDepartment(self, businessController, university.departments[i].curriculums, function () {

                            university.departments.splice(departmentIndex, 1);
                            next();
                        })
                    }
                }
            }

        }).begin(function (key, businessController, operation) {

            operation.callback(function (response) {

                response.deleted = true;
            }).apply();
        }).when('ModelObjectMapping');
    };
});
