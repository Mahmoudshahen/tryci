/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var LogicalOperators = require('beamjs').LogicalOperators;
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var Curriculum = require('../models/curriculum.js').curriculum;

module.exports.deletefromcurriculum = behaviour({

    name: 'deletefromcurriculum',
    version: '1',
    path: "/curriculum/delete/in",
    type: 'database_with_action',
    parameters: {

        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        user: {
            key: 'user',
            type: 'middleware'
        },
        curriculumid: {
            key: "curriculumid",
            type: 'body'
        }

    },
    returns: {

        deleted: {

            type: 'body'
        }
    }
}, function(init) {

    return function() {

        var self = init.apply(this, arguments).self();
        var error = null;
        self.begin('ErrorHandling', function(key, businessController, operation) {

            businessController.modelController.save(function(er) {

                operation.error(function(e) {

                    return error || er || e;
                }).apply();
            });
        });

        if (typeof self.parameters.curriculumid !== 'number' || self.parameters.curriculumid.length === 0) {

            error = new Error('Invalid curriculum Id');
            error.code = 401;
            return;
        }
        self.begin('Delete', function(key, businessController, operation) {

            operation.query([new QueryExpression({

                    fieldName: '_id',
                    comparisonOperator: ComparisonOperators.EQUAL,
                    fieldValue: self.parameters.curriculumid
                })])
                .entity(new Curriculum())
                .callback(function(curriculums, e) {

                    if (e) {
                        error = e;
                        return;
                    }

                })
                .apply();
        }).begin('ModelObjectMapping', function(key, businessController, operation) {

            operation.callback(function(response) {


                response.deleted = true;
            }).apply();
        });
    };
});
