/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var validator = require('email-validator');
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var User = require('../models/user.js').user;
var Utils = require('../utils/error_messages.js');


module.exports.completeregister = behaviour({

  name: 'completeregister',
  version: '1',
  path: '/completeregister',
  method: 'GET',
  type: 'database_with_action',
  parameters: {

    verify_code: {
      key: 'verify_code',
      type: 'query'
    },
    email: {
      key: 'email',
      type: 'query'
    }, 
    token: {
      key: 'X-Access-Token',
      type: 'header'
    }
  },
  returns: {

    email: {

      type: 'body'
    },
    name: {

      type: 'body'
    },
    registered: {

      type: 'body'
    }
  }
}, function(init) {

  return function() {

    var self = init.apply(this, arguments).self();
    var user = null;
    var error = null;
    self.begin('ErrorHandling', function(key, businessController, operation) {

      businessController.modelController.save(function(er) {

        operation.error(function(e) {

          return error || er || e;
        }).apply();
      });
    });

    if (typeof self.parameters.email !== 'string' || self.parameters.email.length === 0 || !validator.validate(self.parameters.email) ||
      typeof self.parameters.verify_code !== 'string' || self.parameters.verify_code.length === 0) {

      error = new Error('Invalid parameters');
      error.code = 401;
      return;
    }

    self.begin('Query', function(key, businessController, operation) {

      operation
        .query([new QueryExpression({

            fieldName: 'email',
            comparisonOperator: ComparisonOperators.EQUAL,
            fieldValue: self.parameters.email
          }),

        ])
        .entity(new User())
        .callback(function(users, e) {

          if (e) error = e;
          if (Array.isArray(users) && users.length == 1)
            user = users[0];
          if (!user) {

            error = new Error(Utils.ERROR.REGISTER.INVALID_EMAIL);
            error.code = 401;
            return;
          }
          if (user && user.verify_code !== self.parameters.verify_code) {

            error = new Error(Utils.ERROR.REGISTER.VERIFY_CODE_EXPIRED);
            error.code = 401;
            return;
          }

          if (user) {
            user.verify_code = "";
            user.verified = true
          }

          businessController.modelController.save(function(error) {
            if (error)
              console.log(error);
          });

        }).apply();
    }).begin('ModelObjectMapping', function(key, businessController, operation) {

      operation.callback(function(response) {

        if (user) {
          response.email = user.email;
          response.name = user.name;
          response.registered = user && true;
        }
      }).apply();
    });
  };
});
