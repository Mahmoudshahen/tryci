/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var Utils = require('../utils/error_messages.js');
var University = require('../models/university.js').university;
var Deletecurriculum = require('./delete_from_curriculums.js').deletefromcurriculum;
module.exports.deletecurriculum = behaviour({

    name: 'deletecurriculum',
    version: '1',
    path: '/curriculum/delete',
    method: 'POST',
    type: 'database_with_action',
    parameters: {

        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        user: {
            key: 'user',
            type: 'middleware'
        },
        departmentid: {
            key:"departmentid",
            type: 'body'
        },
        universityid: {
            key: "universityid",
            type: 'body'
        },
        curriculumid: {
            key: "curriculumid",
            type: 'body'
        }

    },
    returns: {

        deleted: {

            type: 'body'
        }
    }
}, function (init) {

    return function () {

        var self = init.apply(this, arguments).self();
        var user = null;
        var error = null;
        self.begin('ErrorHandling', function (key, businessController, operation) {

            businessController.modelController.save(function (er) {

                operation.error(function (e) {

                    return error || er || e;
                }).apply();
            });
        });

        if (typeof self.parameters.universityid !== 'number' || self.parameters.universityid === 0) {

            error = new Error('Invalid university Id');
            error.code = 400;
            return;
        }
        if (typeof self.parameters.departmentid !== 'number' || self.parameters.departmentid === 0) {

            error = new Error('Invalid department Id');
            error.code = 400;
            return;
        }
        if (typeof self.parameters.curriculumid !== 'number' || self.parameters.curriculumid === 0) {

            error = new Error('Invalid curriculum Id');
            error.code = 400;
            return;
        }
        if (typeof self.parameters.user.type !== 'number' || self.parameters.user.type !== 0) {

            error = new Error(Utils.ERROR.ADMIN.INVALID_ACCESS);
            error.code = 401;
            return;
        }
        self.use(function (key, businessController, next) {

            var deletecurriculum = new Deletecurriculum({
                type: 1,
                priority: 0,
                inputObjects: {
                    curriculumid: self.parameters.curriculumid
                }
            });
            self.mandatoryBehaviour = deletecurriculum;
            businessController.runBehaviour(deletecurriculum, null, function (response, err) {

                if (err) {
                    error = err;
                    return;
                }
               next();
            });

        }).begin(function (key, businessController, operation) {

            operation.query([new QueryExpression({

                fieldName: '_id',
                comparisonOperator: ComparisonOperators.EQUAL,
                fieldValue: self.parameters.universityid
            })])
                .entity(new University())
                .callback(function (universities, e) {

                    if (e) {
                        error = e;
                        return;
                    }
                    if (universities[0]) {
                        for (var i = 0; i < universities[0].departments.length; i++) {
                            for (var j = 0; j < universities[0].departments[i].curriculums.length; j++) {

                                if (universities[0].departments[i].curriculums[j]._id === self.parameters.curriculumid) {
                                    universities[0].departments[i].curriculums.splice(j, 1);
                                    break;
                                }
                            }

                        }
                    }
                })
                .apply();
        }).when('Query')
            .begin('ModelObjectMapping', function (key, businessController, operation) {

                operation.callback(function (response) {

                    response.deleted = true;
                }).apply();
            });
    };
});
