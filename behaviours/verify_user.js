/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var validator = require('email-validator');
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var User = require('../models/user.js').user;
var Utils = require('../utils/error_messages.js');


module.exports.verifyuser = behaviour({

    name: 'verifyuser',
    version: '1',
    path: '/verifyuser',
    method: 'POST',
    type: 'database_with_action',
    parameters: {

        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        user: {
            key: 'user',
            type: 'middleware'
        },
        email: {
            key: 'email',
            type: 'body'
        }
    },
    returns: {

        verified: {

            type: 'body'
        }
    }
}, function (init) {

    return function () {

        var self = init.apply(this, arguments).self();
        var user = null;
        var error = null;
        self.begin('ErrorHandling', function (key, businessController, operation) {

            businessController.modelController.save(function (er) {

                operation.error(function (e) {

                    return error || er || e;
                }).apply();
            });
        });

        if (typeof self.parameters.email !== 'string' || self.parameters.email.length === 0) {

            error = new Error('Invalid email');
            error.code = 401;
            return;
        }
        if (typeof self.parameters.user.type !== 'number' || self.parameters.user.type !== 0) {

            error = new Error(Utils.ERROR.ADMIN.INVALID_ACCESS);
            error.code = 401;
            return;
        }
        self.begin('Query', function (key, businessController, operation) {

            operation
                .query([new QueryExpression({

                    fieldName: 'email',
                    comparisonOperator: ComparisonOperators.EQUAL,
                    fieldValue: self.parameters.email
                })])
                .entity(new User())
                .callback(function (users, e) {

                    if (e) error = e;
                    if (Array.isArray(users) && users.length == 1)
                        user = users[0];
                    if (!user) {

                        error = new Error(Utils.ERROR.REGISTER.INVALID_EMAIL);
                        error.code = 401;
                        return;
                    }

                    if (user) {
                        user.verify_code = "";
                        user.verified = true
                    }

                }).apply();
        }).begin('ModelObjectMapping', function (key, businessController, operation) {

            operation.callback(function (response) {

                if (user) {

                    response.verified = true;
                }
            }).apply();
        });
    };
});
