/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var Package = require('../models/package.js').package;
var Utils = require('../utils/error_messages.js');

module.exports.deletepackage = behaviour({

    name: 'deletepackage',
    version: '1',
    path: '/deletepackage',
    method: 'POST',
    type: 'database_with_action',
    parameters: {

        id: {
            key: 'id',
            type: 'body'
        },
        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        user: {
            key: 'user',
            type: 'middleware'
        }

    },
    returns: {

        deleted: {

            type: 'body'
        }
    }
}, function(init) {

    return function() {

        var self = init.apply(this, arguments).self();
        var error = null;
        self.begin('ErrorHandling', function(key, businessController, operation) {

            businessController.modelController.save(function(er) {

                operation.error(function(e) {

                    return error || er || e;
                }).apply();
            });
        });

        if (typeof self.parameters.id !== 'number' || self.parameters.id.length === 0) {

            error = new Error('Invalid curriculum Id');
            error.code = 401;
            return;
        }
        if (typeof self.parameters.user.type !== 'number' || self.parameters.user.type.length === 0) {

            error = new Error(Utils.ERROR.ADMIN.INVALID_ACCESS);
            error.code = 401;
            return;
        }
        self.begin('Delete', function(key, businessController, operation) {

            operation.query([new QueryExpression({

                    fieldName: '_id',
                    comparisonOperator: ComparisonOperators.EQUAL,
                    fieldValue: self.parameters.id
                })])
                .entity(new Package())
                .callback(function(packages, e) {

                    if (e) {
                        error = e;
                        return;
                    }

                })
                .apply();
        }).begin('ModelObjectMapping', function(key, businessController, operation) {

            operation.callback(function(response) {


                response.deleted = true;
            }).apply();
        });
    };
});
