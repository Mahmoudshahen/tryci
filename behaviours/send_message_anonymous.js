/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var User = require('../models/user.js').user;
var Anonymoususer = require('../models/anonymous_user').anonymoususer;

module.exports.sendmessageanonymous = behaviour({
  name: 'sendmessageanonymous',
  version: '1',
  path: '/feedback/sendmessageanonymous',
  method: 'POST',
  parameters: {

    message: {
      key: 'message',
      type: 'body'
    },
    sessionid: {
      key: 'sessionid',
      type: 'body'
    },
    token: {
      key: 'X-Access-Token',
      type: 'header'
    }
  },
  returns: {

    sent: {
      type: 'body'
    }
  }
}, function (init) {

  return function () {
    var self = init.apply(this, arguments).self();
    var users = null;
    var error = null;
    
    self.begin('ErrorHandling', function (key, businessController, operation) {

      businessController.modelController.save(function (er) {

        operation.error(function (e) {

          return error || er || e;
        }).apply();
      });
    });
    if (typeof self.parameters.sessionid !== 'number' || self.parameters.sessionid.length === 0) {

      error = new Error('Invalid session Id');
      error.code = 401;
      return;
    }
    if (typeof self.parameters.message !== 'string' || self.parameters.message.length === 0) {

      error = new Error('Invalid message');
      error.code = 401;
      return;
    }



    self.begin('Query', function (key, businessController, operation) {

      operation.query()
        .entity(new Anonymoususer())
        .callback(function (_users, e) {
          users = _users;
          if (e) { error = e; return; };

          if (users && users.length > 0) {

            users[0].messages.push({
              _id: 0,
              message: self.parameters.message,
              sent_time: new Date(),
              user_type: 2,
              sessionid: self.parameters.sessionid
            });
          }

        }).apply();
    }).if(function () {

      if (!(users && users.length > 0)) {

        return true;
      }
      return false;
    }).begin('Insert', function (key, businessController, operation) {

      operation.entity(new Anonymoususer()).objects({
        messages: [{

          _id: self.parameters.sessionid,
          message: self.parameters.message,
          sent_time: new Date(),
          user_type: 2,
          sessionid: self.parameters.sessionid

        }]
      }).callback(function (users, e) {

        if (e) error = e;
      }).apply();
    }).begin('ModelObjectMapping', function (key, businessController, operation) {

      operation.callback(function (response) {

        response.sent = true;
      }).apply();
    });
  }
});
