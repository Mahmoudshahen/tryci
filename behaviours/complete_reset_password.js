/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var User = require('../models/user.js').user;
var Utils = require('../utils/error_messages.js');

module.exports.completeresetpassword = behaviour({

    name: 'completeresetpassword',
    version: '1',
    path: '/completeresetpassword',
    method: 'POST',
    type: 'database_with_action',
    parameters: {

        email: {
            key: 'email',
            type: 'body'
        },
        newpassword: {
            key: 'newpassword',
            type: 'body'
        },
        resetcode: {
            key: 'resetcode',
            type: 'body'
        },
        host: {
            key: 'hostname',
            type: 'middleware'
        },
        protocol: {
            key: 'protocol',
            type: 'middleware'
        }
    },
    returns: {

        reseted: {

            type: 'body'
        }
    }
}, function (init) {

    return function () {

        var self = init.apply(this, arguments).self();
        var user = null;
        var error = null;
        self.begin('ErrorHandling', function (key, businessController, operation) {

            businessController.modelController.save(function (er) {

                operation.error(function (e) {

                    return error || er || e;
                }).apply();
            });

        });

        if (typeof self.parameters.email !== 'string' || self.parameters.email.length === 0) {

            error = new Error('البريد الإلكتروني غير صحيح');
            error.code = 401;
            return;
        }
        if (typeof self.parameters.newpassword !== 'string' || self.parameters.newpassword.length === 0) {

            error = new Error('كلمة المرور الجديدة غير صحيحة');
            error.code = 401;
            return;
        }
        if (typeof self.parameters.resetcode !== 'string' || self.parameters.resetcode.length === 0) {

            error = new Error('كود إعاده كلمة المور غير صحيح');
            error.code = 401;
            return;
        }

        self.begin('Query', function (key, businessController, operation) {

            operation.query([new QueryExpression({

                fieldName: 'email',
                comparisonOperator: ComparisonOperators.EQUAL,
                fieldValue: self.parameters.email
            })]).entity(new User()).callback(function (users, e) {

                if (e) error = e;
                user = Array.isArray(users) && users[0];
                if (user && user.reset_code && user.reset_code.length > 0) {
                    if ((((new Date()) - user.reset_expiredate) / (1000 * 60 * 60)) > 24) {

                        error = new Error(Utils.ERROR.RESET_PASSWORD.CODE_EXPIRED);
                        error.code = 401;
                        return;
                    }
                    if (user.reset_code !== null && user.reset_code === self.parameters.resetcode) {
                        user.password = self.parameters.newpassword;
                    }
                    user.reset_code = "";
                } else {
                    error = new Error('تم إستخدا كود التفعيل');
                    error.code = 401;
                    return;
                }

            }).apply();
        }).begin('ModelObjectMapping', function (key, businessController, operation) {

            operation.callback(function (response) {

                if (user) {

                    response.reseted = true;
                } else {
                    response.reseted = false;
                }

            }).apply();
        });
    };
});
