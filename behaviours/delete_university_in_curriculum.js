/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var Curriculum = require('../models/curriculum.js').curriculum;

module.exports.deleteuniversityincurriculum = behaviour({

    name: 'deleteuniversityincurriculum',
    version: '1',
    path: '/deleteuniversityincurriculum',
    method: 'POST',
    parameters: {

        curriculumid: {
            key: 'curriculumid',
            type: 'body'
        },
        token: {
            key: 'X-Access-Token',
            type: 'header'
        }
    },
    returns: {

        deleted: {
            type: 'body'
        }
    }
}, function (init) {

    return function () {
        var self = init.apply(this, arguments).self();
        var curriculum = null;
        var error = null;
        self.begin('ErrorHandling', function (key, businessController, operation) {

            businessController.modelController.save(function (er) {

                operation.error(function (e) {

                    return error || er || e;
                }).apply();
            });
        });
        if (typeof self.parameters.curriculumid !== 'number' || self.parameters.curriculumid.length === 0) {
            error = new Error("invalid curriculum id");
            error.code = 401;
            return;
        }

        self.begin('Query', function (key, businessController, operation) {

            operation.query([new QueryExpression({
                fieldName: '_id',
                comparisonOperator: ComparisonOperators.EQUAL,
                fieldValue: self.parameters.curriculumid
            })])
                .entity(new Curriculum())
                .callback(function (curriculums, e) {

                    if (e) error = e;

                    if (Array.isArray(curriculums) && curriculums.length === 1) {
                        curriculum = curriculums[0];
                        if (curriculum.university) {
                            curriculum.university = undefined;
                        }
                    }


                }).apply();
        }).begin('ModelObjectMapping', function (key, businessController, operation) {

            operation.callback(function (response) {

                response.deleted = true;
            }).apply();
        });
    }
});
