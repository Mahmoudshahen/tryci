/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var University = require('../models/university.js').university;
var Deletefromuniversity = require('./delete_from_university.js').deletefromuniversity;
var Utils = require('../utils/error_messages.js');

module.exports.deleteuniversity = behaviour({

    name: 'deleteuniversity',
    version: '1',
    path: '/universities/delete',
    method: 'POST',
    type: 'database_with_action',
    parameters: {

        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        user: {
            key: 'user',
            type: 'middleware'
        },
        id: {
            key: 'id',
            type: 'body'
        },

    },
    returns: {

        deleted: {

            type: 'body'
        }
    }
}, function(init) {

    return function() {

        var self = init.apply(this, arguments).self();
        var university = null;
        var error = null;
        self.begin('ErrorHandling', function(key, businessController, operation) {

            businessController.modelController.save(function(er) {

                operation.error(function(e) {

                    return error || er || e;
                }).apply();
            });
        });

        if (typeof self.parameters.id !== 'number' || self.parameters.id.length === 0) {

            error = new Error('Invalid Id');
            error.code = 400;
            return;
        }
        if (typeof self.parameters.user !== 'object' || typeof self.parameters.user._id !== 'number') {

            error = new Error('Invalid user');
            error.code = 401;
            return;
        }
        if (typeof self.parameters.user.type !== 'number' || self.parameters.user.type !== 0) {

            error = new Error(Utils.ERROR.ADMIN.INVALID_ACCESS);
            error.code = 401;
            return;
        }
        self.begin('Query', function(key, businessController, operation) {

            operation.query([new QueryExpression({

                    fieldName: '_id',
                    comparisonOperator: ComparisonOperators.EQUAL,
                    fieldValue: self.parameters.id
                })])
                .entity(new University())
                .callback(function(_universities, e) {

                    if (e) { error = e; return; }
                    if (Array.isArray(_universities) && _universities.length > 0)
                        university = _universities[0];
                    else {
                        error = new Error('not exist');
                        return;
                    }
                })
                .apply();
        }).use(function(key, businessController, next) {

            var deletefromuniversity = new Deletefromuniversity({
                type: 1,
                priority: 0,
                inputObjects: {
                    university: university
                }
            });
            self.mandatoryBehaviour = deletefromuniversity;
            businessController.runBehaviour(deletefromuniversity, null, function(response, err) {

                if (err) {
                    error = err;
                    return;
                }

                next();
            });


        }).begin(function(key, businessController, operation) {

            operation.callback(function(response) {

                response.deleted = true;
            }).apply();
        }).when('ModelObjectMapping');
    };
});