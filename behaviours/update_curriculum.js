/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var Curriculum = require('../models/curriculum.js').curriculum;
var Getuniversity = require('./get_university.js').getuniversity;
var Updateuserscurriculums = require('./update_users_curriculums').updateuserscurriculums;
var Deletecurriculumfromuniversity = require('./delete_curriculum_from_university').deletecurriculumfromuniversity;
var Deletecurriculumfrompackage = require('./delete_curriculum_from_package').deletecurriculumfrompackage;
var Addcurriculumtopackage = require('./add_curriculum_to_package').addcurriculumtopackage;

var Utils = require('../utils/error_messages.js');

module.exports.updatecurriculum = behaviour({

    name: 'updatecurriculum',
    version: '1',
    path: '/curriculums/update',
    method: 'POST',
    type: 'database_with_action',
    parameters: {
        id: {
            key: 'id',
            type: 'body'
        },
        level: {
            key: 'level',
            type: 'body'
        },
        code: {
            key: 'code',
            type: 'body'
        },
        name: {
            key: 'name',
            type: 'body'
        },
        subject: {
            key: 'subject',
            type: 'body'
        },
        links: {
            key: 'links',
            type: 'body'
        },
        links_v2: {
            key: "links_v2",
            type: "body"
        },
        duration: {
            key: 'duration',
            type: 'body'
        },
        description: {
            key: 'description',
            type: 'body'
        },
        instructor: {
            key: 'instructor',
            type: 'body'
        },
        media: {
            key: 'media',
            type: 'body'
        },
        features: {
            key: 'features',
            type: 'body'
        },
        socialmedia: {
            key: 'socialmedia',
            type: 'body'
        },
        universityid: {

            key: 'universityid',
            type: 'body'
        },
        departmentid: {
            key: 'departmentid',
            type: 'body'
        },
        packageid: {
            key: 'packageid',
            type: 'body'
        },
        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        user: {
            key: 'user',
            type: 'middleware'
        }
    },
    returns: {

        updated: {
            key: 'updated',
            type: 'body'
        }
    }
}, function (init) {

    return function () {

        var self = init.apply(this, arguments).self();
        var curriculum = null;
        var university = null;
        var error = null;
        if (self.parameters.user && self.parameters.user.type !== 0) {

            error = new Error(Utils.ERROR.ADMIN.INVALID_ACCESS);
            error.code = 401;
            return;
        }
        if (typeof self.parameters.universityid !== "number" || self.parameters.universityid.length === 0) {

            error = new Error('Invalid university Id');
            error.code = 401;
            return;
        }
        if (typeof self.parameters.departmentid !== "number" || self.parameters.departmentid.length === 0) {

            error = new Error('Invalid department Id');
            error.code = 401;
            return;
        }
        if (typeof self.parameters.packageid !== "number" || self.parameters.packageid.length === 0) {

            error = new Error('Invalid package Id');
            error.code = 401;
            return;
        }

        self.begin('ErrorHandling', function (key, businessController, operation) {

            businessController.modelController.save(function (er) {

                operation.error(function (e) {

                    return error || er || e;
                }).apply();
            }, [university, curriculum]);
        });

        self.begin('Query', function (key, businessController, operation) {

            operation.query([new QueryExpression({
                fieldName: '_id',
                comparisonOperator: ComparisonOperators.EQUAL,
                fieldValue: self.parameters.id
            })]).entity(new Curriculum()).callback(function (curriculums, e) {

                curriculum = Array.isArray(curriculums) && curriculums.length === 1 && curriculums[0];

                if (e) { error = e; return; }
                if (curriculum) {

                    curriculum.level = self.parameters.level;
                    curriculum.code = self.parameters.code;
                    curriculum.name = self.parameters.name;
                    curriculum.subject = self.parameters.subject;
                    curriculum.links = self.parameters.links;
                    if (Array.isArray(self.parameters.links_v2)) {
                        for (var i = 0; i < self.parameters.links_v2.length; i++) {
                            self.parameters.links_v2[i]._id = 0;
                        }
                    }
                    curriculum.links_v2 = self.parameters.links_v2;
                    curriculum.duration = self.parameters.duration;
                    curriculum.description = self.parameters.description;
                    curriculum.instructor = self.parameters.instructor;
                    curriculum.media = self.parameters.media;
                    curriculum.features = self.parameters.features;
                    curriculum.social_media = self.parameters.socialmedia;
                }
            }).apply();
        }).use(function (key, businessController, next) {

            var getuniversity = new Getuniversity({

                type: 1,
                priority: 0,
                inputObjects: {
                    universityid: self.parameters.universityid
                }
            });
            self.mandatoryBehaviour = getuniversity;
            businessController.runBehaviour(getuniversity, null, function (response, err) {

                if (err) {
                    error = err;
                    next();
                    return;
                }
                if (response) {
                    university = response.university;
                    if (!university) {
                        error = new Error('Can not find university');
                        next();
                        return;
                    }
                    var deletecurriculumfromuniversity = new Deletecurriculumfromuniversity({

                        type: 1,
                        priority: 0,
                        inputObjects: {
                            curriculumid: curriculum._id,
                            universityid: curriculum.university.id
                        }
                    });
                    self.mandatoryBehaviour = deletecurriculumfromuniversity;
                    businessController.runBehaviour(deletecurriculumfromuniversity, null, function (response, err) {

                        if (err) {
                            error = err;
                            next();
                            return;
                        }
                        if (response) {


                        }
                        for (var i = 0; i < university.departments.length; i++) {

                            if (university.departments[i]._id === self.parameters.departmentid) {

                                university.departments[i].curriculums.push({
                                    level: self.parameters.level,
                                    code: self.parameters.code,
                                    name: self.parameters.name,
                                    _id: curriculum._id
                                });

                                curriculum.university.id = self.parameters.universityid;
                                curriculum.university.name = university.name;
                                curriculum.university.branch = university.branch;
                                curriculum.university.department_id = self.parameters.departmentid;
                                curriculum.university.department_name = university.departments[i].name;
                                break;
                            }
                        }

                        var deletecurriculumfrompackage = new Deletecurriculumfrompackage({

                            type: 1,
                            priority: 0,
                            inputObjects: {
                                curriculumid: curriculum._id,
                                user: self.parameters.user
                            }
                        });
                        self.mandatoryBehaviour = deletecurriculumfrompackage;
                        businessController.runBehaviour(deletecurriculumfrompackage, null, function (response, err) {

                            if (err) {
                                error = err;
                                next();
                                return;
                            }
                            if (response) {


                            }
                            var addcurriculumtopackage = new Addcurriculumtopackage({

                                type: 1,
                                priority: 0,
                                inputObjects: {
                                    curriculumid: curriculum._id,
                                    packageid: self.parameters.packageid,
                                    user: self.parameters.user
                                }
                            });
                            self.mandatoryBehaviour = addcurriculumtopackage;
                            businessController.runBehaviour(addcurriculumtopackage, null, function (response, err) {

                                if (err) {
                                    error = err;
                                    next();
                                    return;
                                }
                                if (response) {


                                }

                                next();

                            });


                        });




                    });

                }

            });

        }).begin(function (key, businessController, operation) {

            operation.callback(function (response) {

                if (curriculum) {

                    response.updated = true;
                }

            }).apply();
        }).when('ModelObjectMapping');
    };
});