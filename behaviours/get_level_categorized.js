/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var LogicalOperators = require('beamjs').LogicalOperators;

var Curriculum = require('../models/curriculum.js').curriculum;

module.exports.getlevel = behaviour({

    name: 'getlevel',
    version: '1',
    path: '/getlevel',
    method: 'POST',
    type: 'database_with_action',
    parameters: {

        levelname: {

            key: 'levelname',
            type: 'body'
        },
        universityid: {
            key: 'universityid',
            type: 'body'
        },
        token: {
            key: 'X-Access-Token',
            type: 'header'
        }

    },
    returns: {

        level: {

            type: 'body'
        }
    }
}, function(init) {

    return function() {

        var self = init.apply(this, arguments).self();
        var error = null;
        var level = {};
        var levelArr = [];
        self.begin('ErrorHandling', function(key, businessController, operation) {

            businessController.modelController.save(function(er) {

                operation.error(function(e) {

                    return error || er || e;
                }).apply();
            });
        });
        var queryExp = [];
        if(self.parameters.levelname) {
            queryExp.push(new QueryExpression({

                fieldName: 'level',
                comparisonOperator: ComparisonOperators.EQUAL,
                fieldValue: self.parameters.levelname,
                logicalOperator: LogicalOperators.AND,
                contextualLevel: 0
            }));
        }
        if(self.parameters.universityid) {

            queryExp.push(new QueryExpression({

                fieldName: 'university.id',
                comparisonOperator: ComparisonOperators.EQUAL,
                fieldValue: self.parameters.universityid,
                logicalOperator: LogicalOperators.AND,
                contextualLevel: 0
            }));
        }
        self.begin('Query', function(key, businessController, operation) {

            operation.query(queryExp).entity(new Curriculum()).callback(function(Curriculums, e) {

                if (e) error = e;

                if (Curriculums !== null && Curriculums.length > 0) {

                    for (var i = 0; i < Curriculums.length; i++) {

                        if (!level[Curriculums[i].university.name + "-" + Curriculums[i].university.branch]) {
                            level[Curriculums[i].university.name + "-" + Curriculums[i].university.branch] = {};
                            level[Curriculums[i].university.name + "-" + Curriculums[i].university.branch].Curriculums = [];
                            level[Curriculums[i].university.name + "-" + Curriculums[i].university.branch].branch = Curriculums[i].university.branch;
                            level[Curriculums[i].university.name + "-" + Curriculums[i].university.branch].university_name = Curriculums[i].university.name;
                            level[Curriculums[i].university.name + "-" + Curriculums[i].university.branch].university_id = Curriculums[i].university.id;
                        }

                        level[Curriculums[i].university.name + "-" + Curriculums[i].university.branch].Curriculums.push({

                            id: Curriculums[i]._id,
                            code: Curriculums[i].code,
                            name: Curriculums[i].name,
                            department_id: Curriculums[i].university.department_id,
                            department_name: Curriculums[i].university.department_name

                        });

                    }

                    for (var key in level)
                        levelArr.push(level[key]);


                }
            }).apply();
        }).begin('ModelObjectMapping', function(key, businessController, operation) {

            operation.callback(function(response) {

                response.level = levelArr;
            }).apply();
        });
    };
});
