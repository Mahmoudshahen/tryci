/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var User = require('../models/user.js').user;
var Anonymoususer = require('../models/anonymous_user').anonymoususer;

module.exports.replymessage = behaviour({
    name: 'replymessage',
    version: '1',
    path: '/feedback/replymessage',
    method: 'POST',
    parameters: {

        message: {
            key: 'message',
            type: 'body'
        },
        sessionid: {
            key: 'sessionid',
            type: 'body'
        },
        userid: {
            key: 'userid',
            type: 'body'
        },
        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        user: {
            key: 'user',
            type: 'middleware'
        }
    },
    returns: {

        sent: {
            type: 'body'
        }
    }
}, function (init) {

    return function () {
        var self = init.apply(this, arguments).self();
        var users = null;
        var error = null;

        self.begin('ErrorHandling', function (key, businessController, operation) {

            businessController.modelController.save(function (er) {

                operation.error(function (e) {

                    return error || er || e;
                }).apply();
            });
        });
        if (typeof self.parameters.user !== 'object' || self.parameters.user.type !== 0) {

            error = new Error('Invalid admin');
            error.code = 401;
            return;
        }
        if (typeof self.parameters.message !== 'string' || self.parameters.message.length === 0) {

            error = new Error('Invalid message');
            error.code = 401;
            return;
        }


        

        var queryExp = null;
        var userType = null;
        if (self.parameters.userid) {
            queryExp = [new QueryExpression({
                fieldName: '_id',
                comparisonOperator: ComparisonOperators.EQUAL,
                fieldValue: self.parameters.userid
            })];
            userType = new User();
        } else {
            
            if (typeof self.parameters.sessionid !== 'number' || self.parameters.sessionid.length === 0) {

                error = new Error('Invalid session id');
                error.code = 401;
                return;
            }
            userType = new Anonymoususer();
        }


        self.begin('Query', function (key, businessController, operation) {

            operation.query(queryExp)
                .entity(userType)
                .callback(function (_users, e) {
                    users = _users;
                    if (e) { error = e; return; };

                    if (users && users.length > 0) {

                        users[0].messages.push({
                            _id: 1,
                            message: self.parameters.message,
                            sent_time: new Date(),
                            user_type: self.parameters.user.type,
                            sessionid: self.parameters.sessionid,
                            name: self.parameters.user.name
                        });
                    }

                }).apply();
        }).begin('ModelObjectMapping', function (key, businessController, operation) {

            operation.callback(function (response) {

                response.sent = true;
            }).apply();
        });
    }
});
