/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var Preference = require('../models/preference.js').preference;

module.exports.getpreference = behaviour({

    name: 'getpreference',
    version: '1',
    path: '/getpreference',
    method: 'GET',
    type: 'database_with_action',
    parameters: {
		
        token: {
            key: 'X-Access-Token',
            type: 'header'
        }

    },
    returns: {

        preference: {
            key: 'preference',
            type: 'body'
        }
    }
}, function(init) {

    return function() {

        var self = init.apply(this, arguments).self();
        var preference = null;
        var error = null;
        self.begin('ErrorHandling', function(key, businessController, operation) {

            operation.error(function(e) {

                return error || e;
            }).apply();
        });

        self.begin('Query', function(key, businessController, operation) {

            operation.query()
                .entity(new Preference())
                .callback(function(preferences, e) {

                    if (e) error = e;

                    preference = Array.isArray(preferences) && preferences.length > 0 && preferences[0];
                }).apply();
        }).begin('ModelObjectMapping', function(key, businessController, operation) {

            operation.callback(function(response) {

                response.preference = preference;

            }).apply();
        });
    };
});
