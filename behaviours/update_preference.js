/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var Preference = require('../models/preference.js').preference;
var Utils = require('../utils/error_messages.js');

var sleep = function(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds) {
            break;
        }
    }
}
module.exports.updatepreference = behaviour({

    name: 'updatepreference',
    version: '1',
    path: '/updatepreference',
    method: 'POST',
    type: 'database_with_action',
    parameters: {

        facebook: {
            key: 'facebook',
            type: 'body'
        },
        twitter: {
            key: 'twitter',
            type: 'body'
        },
        email: {
            key: 'email',
            type: 'body'
        },
        youtube: {
            key: 'youtube',
            type: 'body'
        },
        whatsapp: {
            key: 'whatsapp',
            type: 'body'
        },
        telegram: {
            key: 'telegram',
            type: 'body'
        },
        paymentsnumbers: {
            key: 'paymentsnumbers',
            type: 'body'
        },
        contactnumbers: {
            key: 'contactnumbers',
            type: 'body'
        },
        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        user: {
            key: 'user',
            type: 'middleware'
        }
    },
    returns: {

        updated: {
            key: 'updated',
            type: 'body'
        }
    }
}, function(init) {

    return function() {

        var self = init.apply(this, arguments).self();
        var preference = null;
        var error = null;
        self.begin('ErrorHandling', function(key, businessController, operation) {
            businessController.modelController.save(function(er) {

                operation.error(function(e) {

                    return error || er || e;
                }).apply();
            });

        });
        if (typeof self.parameters.user !== 'object' || self.parameters.user.type !== 0) {

            error = new Error(Utils.ERROR.ADMIN.INVALID_ACCESS);
            error.code = 401;
            return;
        }
        self.begin('Query', function(key, businessController, operation) {

            operation.query()
                .entity(new Preference())
                .callback(function(preferences, e) {

                    if (e) error = e;

                    preference = Array.isArray(preferences) && preferences.length > 0 && preferences[0];

                    if (preference) {
                        for (var i = 0; i < self.parameters.paymentsnumbers.length; i++) {
                            if (!self.parameters.paymentsnumbers[i].hasOwnProperty('_id')) {
                                var date = new Date();
                                self.parameters.paymentsnumbers[i]._id = "" + date.getFullYear() + date.getMonth() + date.getDay() + date.getHours() + date.getMinutes() + date.getMilliseconds();
                                sleep(5);
                            }
                        }
                        for (var i = 0; i < self.parameters.whatsapp.length; i++) {
                            if (!self.parameters.whatsapp[i].hasOwnProperty('_id')) {
                                var date = new Date();
                                self.parameters.whatsapp[i]._id = "" + date.getFullYear() + date.getMonth() + date.getDay() + date.getHours() + date.getMinutes() + date.getMilliseconds();
                                sleep(5);
                            }
                        }

                        preference.email = self.parameters.email;
                        preference.facebook = self.parameters.facebook;
                        preference.twitter = self.parameters.twitter;
                        preference.youtube = self.parameters.youtube;
                        preference.whatsapp = self.parameters.whatsapp;
                        preference.telegram = self.parameters.telegram;
                        preference.payments_numbers = self.parameters.paymentsnumbers;
                        preference.contact_numbers = self.parameters.contactnumbers;
                    } else {
                        error = new Error('الأعدادات غير موجودة');
                        error.code = 401;
                        return;
                    }

                }).apply();

        }).begin('ModelObjectMapping', function(key, businessController, operation) {

            operation.callback(function(response) {

                if (preference) {

                    response.updated = true;
                }


            }).apply();
        });
    };
});