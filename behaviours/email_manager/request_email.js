var nodeMailer = require('nodemailer');
// must use   "email-templates": "2.6.1" more recent versions doesn't work this way
var EmailTemplate = require('email-templates').EmailTemplate;
var settings = require('./settings.json');
var pathlib = require('path');
var sender = settings.email_sender;
var password = settings.email_password;
var transporter = nodeMailer.createTransport(sender + ':' + password + '@' + settings.email_server);
module.exports.frontend_port = settings.frontend_port;
module.exports.send = function(email, callback) {


    var dirPath = pathlib.resolve("./behaviours/email_templates");
    var path = dirPath + '/' + email.path;
    var from = settings.from;
    var to = email.to;
    var subject = email.subject;
    var dataObj = email.dataObj;
    if (typeof path !== 'string' || path.length === 0 || typeof from !== 'string' || typeof to !== 'string' ||
        to.length === 0 || typeof subject !== 'string') {

        callback(null, new Error('Invalid email parameters'));
        return;
    }
    if (!dataObj || typeof dataObj !== 'object') {

        callback(null, new Error('Invalid email parameters'));
        return;
    }
    var template_instance = new EmailTemplate(path);
    var sendMessage = transporter.templateSender(template_instance, {

        from: from
    });
    sendMessage({

        to: to,
        subject: subject
    }, dataObj, function(error, response) {

        callback(response, error);
    });
};