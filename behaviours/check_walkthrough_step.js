/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var User = require('../models/user.js').user;

module.exports.checkwalkthroughstep = behaviour({

    name: 'checkwalkthroughstep',
    version: '1',
    path: '/users/checkwalkthroughstep',
    method: 'POST',
    type: 'database_with_action',
    parameters: {

        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        step: {
            key: 'step',
            type: 'body'
        },
        user: {
            key: 'user',
            type: 'middleware'
        }
    },
    returns: {

        exist: {

            type: 'body'
        }
    }
}, function (init) {

    return function () {

        var self = init.apply(this, arguments).self();
        var user = null;
        var error = null;
        var exist = false;

        self.begin('ErrorHandling', function (key, businessController, operation) {

            businessController.modelController.save(function (er) {

                operation.error(function (e) {

                    return error || er || e;
                }).apply();
            }, [self.parameters.user]);
        });
        if (typeof self.parameters.step !== 'object') {
            error = new Error('invalid step');
            error.code = 401;
            return;
        }

        self.begin('Query', function (key, businessController, operation) {

            operation
                .query([new QueryExpression({

                    fieldName: '_id',
                    comparisonOperator: ComparisonOperators.EQUAL,
                    fieldValue: self.parameters.user._id
                })])
                .entity(new User())
                .callback(function (users, e) {

                    if (e) error = e;

                    user = Array.isArray(users) && users.length === 1 && users[0];
                    if (user) {

                        for (var i = 0; i < user.walkthrough.length; i++) {
                            if (user.walkthrough[i].datastep === self.parameters.step.datastep) {
                                exist = true;
                                break;
                            }
                        }

                    }

                }).apply();
        }).begin('ModelObjectMapping', function (key, businessController, operation) {

            operation.callback(function (response) {

                response.exist = exist;
            }).apply();
        });
    };
});
