/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;

var Utils = require('../utils/error_messages.js');

var Preference = require('../models/preference.js').preference;
var sleep = function (milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds) {
            break;
        }
    }
}
module.exports.addpreference = behaviour({

    name: 'addpreference',
    version: '1',
    path: '/addpreference',
    method: 'POST',
    type: 'database_with_action',
    parameters: {

        facebook: {
            key: 'facebook',
            type: 'body'
        },
        twitter: {
            key: 'twitter',
            type: 'body'
        },
        email: {
            key: 'email',
            type: 'body'
        },
        youtube: {
            key: 'youtube',
            type: 'body'
        },
        whatsapp: {
            key: 'whatsapp',
            type: 'body'
        },
        telegram: {
            key: 'telegram',
            type: 'body'
        },
        paymentsnumbers: {
            key: 'paymentsnumbers',
            type: 'body'
        },
        contactnumbers: {
            key: 'contactnumbers',
            type: 'body'
        },
        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        user: {
            key: 'user',
            type: 'middleware'
        }
    },
    returns: {

        added: {
            key: 'added',
            type: 'body'
        }
    }
}, function (init) {

    return function () {

        var self = init.apply(this, arguments).self();
        var preference = null;
        var error = null;
        self.begin('ErrorHandling', function (key, businessController, operation) {
            businessController.modelController.save(function (er) {

                operation.error(function (e) {

                    return error || er || e;
                }).apply();
            });

        });
        if (typeof self.parameters.user.type !== 'number' || self.parameters.user.type !== 0) {

            error = new Error(Utils.ERROR.ADMIN.INVALID_ACCESS);
            error.code = 401;
            return;
        }
        self.begin('Query', function (key, businessController, operation) {

            operation.query([new QueryExpression({

                fieldName: 'email',
                comparisonOperator: ComparisonOperators.EQUAL,
                fieldValue: self.parameters.email
            })]).entity(new Preference())
                .callback(function (preferences, e) {

                    if (e) error = e;
                    preference = Array.isArray(preferences) && preferences.length > 0 && preferences[0];
                }).apply();
        }).if(function () {

            if (!preference) {
                return true;
            }
            error = new Error('موجود بالفعل');
            error.code = 401;
            return false;
        }).begin('Insert', function (key, businessController, operation) {

            if (Array.isArray(self.parameters.paymentsnumbers)) {

                for (var i = 0; i < self.parameters.paymentsnumbers.length; i++) {

                    var date = new Date();
                    self.parameters.paymentsnumbers[i]._id = "" + date.getFullYear() + date.getMonth() + date.getDay() + date.getHours() + date.getMinutes() + date.getMilliseconds();
                    sleep(5);
                }
            }
            if (Array.isArray(self.parameters.whatsapp)) {

                for (var i = 0; i < self.parameters.whatsapp.length; i++) {

                    var date = new Date();
                    self.parameters.whatsapp[i]._id = "" + date.getFullYear() + date.getMonth() + date.getDay() + date.getHours() + date.getMinutes() + date.getMilliseconds();
                    sleep(5);
                }
            }
            var preferenceObj = {
                email: self.parameters.email,
                facebook: self.parameters.facebook,
                twitter: self.parameters.twitter,
                youtube: self.parameters.youtube,
                whatsapp: self.parameters.whatsapp,
                telegram: self.parameters.telegram,
                payments_numbers: self.parameters.paymentsnumbers,
                contact_numbers: self.parameters.contactnumbers
            }

            operation.entity(new Preference()).objects(preferenceObj).callback(function (preferences, e) {

                if (e) error = e;
                preference = Array.isArray(preferences) && preferences.length > 0 && preferences[0];
            }).apply();

        }).begin('ModelObjectMapping', function (key, businessController, operation) {

            operation.callback(function (response) {

                if (preference)
                    response.added = true;


            }).apply();
        });
    };
});
