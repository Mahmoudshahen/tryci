/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var User = require('../models/user.js').user;
var Utils = require('../utils/error_messages.js');
var LogicalOperators = require('beamjs').LogicalOperators;
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;

module.exports.getunregisteredusers = behaviour({
    name: 'getunregisteredusers',
    version: '1',
    path: '/admin/getunregisteredusers',
    method: 'POST',
    parameters: {

        name: {
            key: 'name',
            type: 'body'
        },
        curriculumid: {
            key: 'curriculumid',
            type: 'body'
        },
        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        user: {
            key: 'user',
            type: 'middleware'
        }
    },
    returns: {

        users: {
            type: 'body'
        }
    }
}, function (init) {

    return function () {
        var self = init.apply(this, arguments).self();
        var res_users = [];
        var error = null;
        self.begin('ErrorHandling', function (key, businessController, operation) {

            operation.error(function (e) {

                return error || e;
            }).apply();
        });
        if (typeof self.parameters.user !== 'object' || self.parameters.user.type !== 0) {

            error = new Error(Utils.ERROR.ADMIN.INVALID_ACCESS);
            error.code = 401;
            return;
        }
        if (typeof self.parameters.curriculumid !== 'number' || self.parameters.curriculumid.length === 0) {

            error = new Error('invalid curriculum id');
            error.code = 401;
            return;
        }

        var queryExp = [];
        if (self.parameters.name) {
            queryExp = [new QueryExpression({
                fieldName: 'name',
                comparisonOperator: ComparisonOperators.EQUAL,
                comparisonOperatorOptions: ComparisonOperators.CASEINSENSITIVECOMPARE,
                fieldValue: new RegExp(self.parameters.name),
                contextualLevel: 0
            }),
            new QueryExpression({
                fieldName: 'email',
                comparisonOperator: ComparisonOperators.EQUAL,
                comparisonOperatorOptions: ComparisonOperators.CASEINSENSITIVECOMPARE,
                fieldValue: new RegExp(self.parameters.name),
                logicalOperator: LogicalOperators.OR,
                contextualLevel: 0
            }),
            new QueryExpression({
                fieldName: 'mobile',
                comparisonOperator: ComparisonOperators.CONTAINS,
                comparisonOperatorOptions: ComparisonOperators.CASEINSENSITIVECOMPARE,
                fieldValue: new RegExp(self.parameters.name),
                logicalOperator: LogicalOperators.OR,
                contextualLevel: 0
            })];
        }


        self.begin('Query', function (key, businessController, operation) {

            operation.query(queryExp).entity(new User()).callback(function (users, e) {

                if (e) { error = e; return; }

                if (Array.isArray(users)) {

                    for (var i = 0; i < users.length; i++) {

                        if (Array.isArray(users[i].curriculums)) {
                            var has = false;
                            for (var j = 0; j < users[i].curriculums.length; j++) {
                                if (users[i].curriculums[j]._id === self.parameters.curriculumid) {
                                    has = true;
                                    break;
                                }
                            }
                            if (!has) {
                                res_users.push({

                                    _id: users[i]._id,
                                    email: users[i].email,
                                    name: users[i].name,
                                    mobile: users[i].mobile,
                                });
                            }
                        }

                    }
                }
            }).apply();
        }).begin('ModelObjectMapping', function (key, businessController, operation) {

            operation.callback(function (response) {

                response.users = res_users;
            }).apply();
        });
    }
});