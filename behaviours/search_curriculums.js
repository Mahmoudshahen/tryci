/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var ComparisonOperators = require('beamjs').ComparisonOperators;
var LogicalOperators = require('beamjs').LogicalOperators;
var QueryExpression = backend.QueryExpression;
var Curriculum = require('../models/curriculum.js').curriculum;

module.exports.searchcurriculum = behaviour({

    name: 'searchcurriculum',
    version: '1',
    path: '/curriculumssearch',
    method: 'POST',
    parameters: {

        name: {
            key: 'name',
            type: 'body'
        },
        token: {
            key: 'X-Access-Token',
            type: 'header'
        }
    },
    returns: {

        universities: {
            key: 'universities',
            type: 'body'
        }
    }
}, function(init) {

    return function() {
        var self = init.apply(this, arguments).self();
        var curriculumsObject = {};
        var curriculumsObjectArr = [];
        var error = null;
        self.begin('ErrorHandling', function(key, businessController, operation) {

            operation.error(function(e) {

                return error || e;
            }).apply();
        });

        self.begin('Query', function(key, businessController, operation) {

            operation.query([new QueryExpression({
                    fieldName: 'name',
                    comparisonOperator: ComparisonOperators.EQUAL,
                    comparisonOperatorOptions: ComparisonOperators.CASEINSENSITIVECOMPARE,
                    fieldValue: new RegExp(self.parameters.name),
                    contextualLevel: 0
                }),
                new QueryExpression({
                    fieldName: 'description',
                    comparisonOperator: ComparisonOperators.EQUAL,
                    comparisonOperatorOptions: ComparisonOperators.CASEINSENSITIVECOMPARE,
                    fieldValue: new RegExp(self.parameters.name),
                    logicalOperator: LogicalOperators.OR,
                    contextualLevel: 0
                }),
                new QueryExpression({
                    fieldName: 'level',
                    comparisonOperator: ComparisonOperators.EQUAL,
                    comparisonOperatorOptions: ComparisonOperators.CASEINSENSITIVECOMPARE,
                    fieldValue: new RegExp(self.parameters.name),
                    logicalOperator: LogicalOperators.OR,
                    contextualLevel: 0
                }),
                new QueryExpression({
                    fieldName: 'code',
                    comparisonOperator: ComparisonOperators.EQUAL,
                    comparisonOperatorOptions: ComparisonOperators.CASEINSENSITIVECOMPARE,
                    fieldValue: new RegExp(self.parameters.name),
                    logicalOperator: LogicalOperators.OR,
                    contextualLevel: 0
                }),
                new QueryExpression({
                    fieldName: 'university.name',
                    comparisonOperator: ComparisonOperators.EQUAL,
                    comparisonOperatorOptions: ComparisonOperators.CASEINSENSITIVECOMPARE,
                    fieldValue: new RegExp(self.parameters.name),
                    logicalOperator: LogicalOperators.OR,
                    contextualLevel: 0
                }),
                new QueryExpression({
                    fieldName: 'university.branch',
                    comparisonOperator: ComparisonOperators.EQUAL,
                    comparisonOperatorOptions: ComparisonOperators.CASEINSENSITIVECOMPARE,
                    fieldValue: new RegExp(self.parameters.name),
                    logicalOperator: LogicalOperators.OR,
                    contextualLevel: 0
                }),
                new QueryExpression({
                    fieldName: 'university.department_name',
                    comparisonOperator: ComparisonOperators.EQUAL,
                    comparisonOperatorOptions: ComparisonOperators.CASEINSENSITIVECOMPARE,
                    fieldValue: new RegExp(self.parameters.name),
                    logicalOperator: LogicalOperators.OR,
                    contextualLevel: 0
                }),
                new QueryExpression({
                    fieldName: 'instructor',
                    comparisonOperator: ComparisonOperators.EQUAL,
                    comparisonOperatorOptions: ComparisonOperators.CASEINSENSITIVECOMPARE,
                    fieldValue: new RegExp(self.parameters.name),
                    logicalOperator: LogicalOperators.OR,
                    contextualLevel: 0
                })])
                .entity(new Curriculum())
                .callback(function(res_curriculums, e) {

                    if (e) error = e;

                    if (res_curriculums !== null && Array.isArray(res_curriculums)) {

                        for (var i = 0; i < res_curriculums.length; i++) {

                            if (!curriculumsObject[res_curriculums[i].university.name + "-" + res_curriculums[i].university.branch]) {
                                curriculumsObject[res_curriculums[i].university.name + "-" + res_curriculums[i].university.branch] = {};
                                curriculumsObject[res_curriculums[i].university.name + "-" + res_curriculums[i].university.branch].curriculums = [];
                                curriculumsObject[res_curriculums[i].university.name + "-" + res_curriculums[i].university.branch].branch = res_curriculums[i].university.branch;
                                curriculumsObject[res_curriculums[i].university.name + "-" + res_curriculums[i].university.branch].university_name = res_curriculums[i].university.name;
                                curriculumsObject[res_curriculums[i].university.name + "-" + res_curriculums[i].university.branch].university_id = res_curriculums[i].university.id;

                            }

                            curriculumsObject[res_curriculums[i].university.name + "-" + res_curriculums[i].university.branch].curriculums.push({

                                id: res_curriculums[i]._id,
                                code: res_curriculums[i].code,
                                name: res_curriculums[i].name,
                                department_id: res_curriculums[i].university.department_id,
                                department_name: res_curriculums[i].university.department_name,
                                icon: res_curriculums[i].media.iconUri

                            });

                        }

                        for (var key in curriculumsObject)
                            curriculumsObjectArr.push(curriculumsObject[key]);


                    }
                }).apply();
        });

        self.begin('ModelObjectMapping', function(key, businessController, operation) {

            operation.callback(function(response) {

                response.universities = curriculumsObjectArr;
            }).apply();
        });
    }
});
