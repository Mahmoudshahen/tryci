/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var behaviour = backend.behaviour();
var Curriculum = require('../models/curriculum.js').curriculum;
var LogicalOperators = require('beamjs').LogicalOperators;
var ComparisonOperators = require('beamjs').ComparisonOperators;
var QueryExpression = backend.QueryExpression;
var Getcurriculumprice = require('./get_curriculum_price.js').getcurriculumprice;

module.exports.getcurriculums = behaviour({

    name: 'getcurriculums',
    version: '1',
    path: '/curriculums/get',
    method: 'POST',
    parameters: {

        name: {
            key: 'name',
            type: 'body'
        },
        token: {
            key: 'X-Access-Token',
            type: 'header'
        },
        user: {
            key: 'user',
            type: 'middleware'
        },
        page: {
            key: 'page',
            type: 'body'
        }
    },
    returns: {

        curriculums: {
            key: 'curriculums',
            type: 'body'
        },
        pagecount: {
            type: 'body'
        }        

    }
}, function (init) {

    return function () {
        var self = init.apply(this, arguments).self();
        var curriculums = [];
        var pagecount = 0;
        var error = null;
        self.begin('ErrorHandling', function (key, businessController, operation) {

            operation.error(function (e) {

                return error || e;
            }).apply();
        });
        var queryExp = null;
        if (self.parameters.name) {
            queryExp = [new QueryExpression({
                fieldName: 'name',
                comparisonOperator: ComparisonOperators.EQUAL,
                comparisonOperatorOptions: ComparisonOperators.CASEINSENSITIVECOMPARE,
                fieldValue: new RegExp(self.parameters.name),
                contextualLevel: 0
            }),
            new QueryExpression({
                fieldName: 'description',
                comparisonOperator: ComparisonOperators.EQUAL,
                comparisonOperatorOptions: ComparisonOperators.CASEINSENSITIVECOMPARE,
                fieldValue: new RegExp(self.parameters.name),
                logicalOperator: LogicalOperators.OR,
                contextualLevel: 0
            }),
            new QueryExpression({
                fieldName: 'level',
                comparisonOperator: ComparisonOperators.EQUAL,
                comparisonOperatorOptions: ComparisonOperators.CASEINSENSITIVECOMPARE,
                fieldValue: new RegExp(self.parameters.name),
                logicalOperator: LogicalOperators.OR,
                contextualLevel: 0
            }),
            new QueryExpression({
                fieldName: 'code',
                comparisonOperator: ComparisonOperators.EQUAL,
                comparisonOperatorOptions: ComparisonOperators.CASEINSENSITIVECOMPARE,
                fieldValue: new RegExp(self.parameters.name),
                logicalOperator: LogicalOperators.OR,
                contextualLevel: 0
            }),
            new QueryExpression({
                fieldName: 'university.name',
                comparisonOperator: ComparisonOperators.EQUAL,
                comparisonOperatorOptions: ComparisonOperators.CASEINSENSITIVECOMPARE,
                fieldValue: new RegExp(self.parameters.name),
                logicalOperator: LogicalOperators.OR,
                contextualLevel: 0
            }),
            new QueryExpression({
                fieldName: 'university.branch',
                comparisonOperator: ComparisonOperators.EQUAL,
                comparisonOperatorOptions: ComparisonOperators.CASEINSENSITIVECOMPARE,
                fieldValue: new RegExp(self.parameters.name),
                logicalOperator: LogicalOperators.OR,
                contextualLevel: 0
            }),
            new QueryExpression({
                fieldName: 'university.department_name',
                comparisonOperator: ComparisonOperators.EQUAL,
                comparisonOperatorOptions: ComparisonOperators.CASEINSENSITIVECOMPARE,
                fieldValue: new RegExp(self.parameters.name),
                logicalOperator: LogicalOperators.OR,
                contextualLevel: 0
            }),
            new QueryExpression({
                fieldName: 'instructor',
                comparisonOperator: ComparisonOperators.EQUAL,
                comparisonOperatorOptions: ComparisonOperators.CASEINSENSITIVECOMPARE,
                fieldValue: new RegExp(self.parameters.name),
                logicalOperator: LogicalOperators.OR,
                contextualLevel: 0
            })];
        }
        var paginate = null;
        if (self.parameters.page && typeof self.parameters.page === 'number') {
            paginate = {
                paginate: true,
                page: self.parameters.page || 1,
                limit: 50,
            };
        }

        self.begin('Query', function (key, businessController, operation) {

            operation.query(queryExp)
                .entity(new Curriculum(paginate))
                .callback(function (_curriculums, e) {

                    if (e) error = e;

                    if (paginate !== null) {
                        pagecount = _curriculums.pageCount;
                        _curriculums = _curriculums.modelObjects;
                    }

                    if (Array.isArray(_curriculums)) {
                        for (var i = 0; i < _curriculums.length; i++) {
                            curriculums.push({
                                code: _curriculums[i].code,
                                duration: _curriculums[i].duration,
                                level: _curriculums[i].level,
                                name: _curriculums[i].name,
                                university: _curriculums[i].university,
                                _id: _curriculums[i]._id,
                                media: _curriculums[i].media,
                                features: _curriculums[i].features,
                                instructor: _curriculums[i].instructor,
                                description: _curriculums[i].description,
                                socialmedia: _curriculums[i].social_media

                            });

                        }
                    }
                }).apply();
        }).begin('ModelObjectMapping', function (key, businessController, operation) {

            operation.callback(function (response) {

                response.curriculums = curriculums;
                response.pagecount = pagecount;
            }).apply();
        });
    }
});
