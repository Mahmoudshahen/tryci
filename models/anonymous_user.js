/*jslint node: true*/
'use strict';

var TimestampsPlugin = require('mongoose-timestamp');
var backend = require('beamjs').backend();
var model = backend.model();

module.exports.anonymoususer = model({

    name: "anonymoususer"
}, {
    
    messages: [{
        name: String,
        message: String,
        sent_time: Date,
        user_type: Number,
        sessionid: Number
    }]

}, [TimestampsPlugin]);
