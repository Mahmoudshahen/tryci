/*jslint node: true*/
'use strict';

var TimestampsPlugin = require('mongoose-timestamp');
var backend = require('beamjs').backend();
var model = backend.model();
module.exports.university = model({

    name: "university"
}, {
    id: Number,
    name: String,
    mobile: String,
    email: String,
    country: String,
    city: String,
    branch: String,
    departments: [{
        id: Number,
        name: String,
        collage: {
            name: String
        },
        curriculums: [{

            id: Number,
            level: String,
            code: String,
            name: String
        }]
    }]
}, [TimestampsPlugin]);
