/*jslint node: true*/
'use strict';

var TimestampsPlugin = require('mongoose-timestamp');
var backend = require('beamjs').backend();
var model = backend.model();
module.exports.curriculum = model({

    name: "curriculum"
}, {
        id: Number,
        level: String,
        code: String,
        name: String,
        subject: String,
        links: [String],
        links_v2: [{

            link: String,
            filename: String,
            name: String,
            deviceCompatability: String // win, mac
            
        }],
        duration: String,
        description: String,
        instructor: String,
        serials: [String],
        serials_mac: [String],
        media: {

            imageUri: String,
            videoUri: String,
            iconUri: String
        },
        features: [String],
        university: {

            id: Number,
            name: String,
            branch: String,
            department_id: Number,
            department_name: String
        },
        social_media: {

            whatsapp: String,
            facebook: String,
            telegram: String,
            youtube: String,
        }
    }, [TimestampsPlugin]);
