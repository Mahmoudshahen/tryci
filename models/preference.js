/*jslint node: true*/
'use strict';

var backend = require('beamjs').backend();
var model = backend.model();

module.exports.preference = model({

  name: "preference"
}, {
    id: Number,
    email: String,
    facebook: String,
    twitter: String,
    youtube: String,
    telegram: String,
    contact_numbers: [String],
    whatsapp: [{
      number: String,
      link: String,
    }],
    payments_numbers: [{
      
      bank_account_number: String,
      bank_name: String,
      bank_account_name: String
    }],
  });
