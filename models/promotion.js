/*jslint node: true*/
'use strict';

var TimestampsPlugin = require('mongoose-timestamp');
var backend = require('beamjs').backend();
var model = backend.model();

module.exports.promotion = model({

  name: "promotion"
}, {
    id: Number,
    name: String,
    discount: Number,

    curriculums: [Number]
  }, [TimestampsPlugin]);
