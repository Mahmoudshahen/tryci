/*jslint node: true*/
'use strict';

var TimestampsPlugin = require('mongoose-timestamp');
var HashedpropertyPlugin = require('mongoose-hashed-property');
var SecretPlugin = require('mongoose-secret');
var backend = require('beamjs').backend();
var model = backend.model();
module.exports.user = model({

    name: "user"
}, {
    id: Number,
    name: String,
    mobile: [String],
    email: String,
    type: Number, //   0 admin,   1 user
    country: String,
    city: String,
    image_uri: String,
    university: {
        name: String,
        branch: String
    },
    birthdate: Date,
    level: String,
    curriculums: [{
        _id: Number,
        enroll_date: Date,
        paid: Boolean,
        serial: String,
        deviceCompatability: String // win, mac

    }],
    messages: [{
        name: String,
        message: String,
        sent_time: Date,
        user_type: Number,
        sessionid: Number
    }],
    verify_code: String,
    verified: Boolean,
    reset_code: String,
    reset_expiredate: Date,
    walkthrough: [{
        _id: Number,
        datastep: Number,
        step_name: String
    }]

}, [TimestampsPlugin, HashedpropertyPlugin, SecretPlugin]);
