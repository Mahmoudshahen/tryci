/*jslint node: true*/
'use strict';

var TimestampsPlugin = require('mongoose-timestamp');
var backend = require('beamjs').backend();
var model = backend.model();

module.exports.package = model({

    name: "package"
}, {
    id: Number,
    name: String,
    type: String, //subscribtion, consumable
    duration: Number,
    cost: Number,
    currency: String,
    curriculums: [Number]
}, [TimestampsPlugin]);
