/*jslint node: true*/
'use strict';
var beam = require('beamjs');

beam.backend(beam.database('mongodb', undefined, 'baset')).app(__dirname + '/behaviours', {

    path: '/api/v1',
    parser: 'json',
    port: 80,
    origins: '*',
    static: {

        path: 'uploads'
    }
})
